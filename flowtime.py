"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
### Calculation of flow time and time compensations between two nodes for the merge-approach. 

def flowtimeMerge(CatchAddLength, t_adj, catch_muid, pipe_adj):
    import math
    #CatchAddlength( length_c, slope, diameters, manning, catch_muid, pipe_muid)
    for row in CatchAddLength:
        if not len(row)==0:
            for pipe in row:
                #define variables
                p_length=pipe[0]
                p_slope=pipe[1]
                p_dia=pipe[2]
                p_manning=pipe[3]
                catch_id=pipe[4]
                p_name=pipe[5]
                #if no or very slow slope assign 0.15m/s as the flow velocity:
                if p_slope==0:
                    V=0.15
                else:
                    p_slope=pipe[1]/float(100)
                    hyd_rad=math.pi*math.pow(p_dia/float(2),2)/(math.pi*p_dia)
                    V=p_manning*math.pow(hyd_rad,float(2)/3)*math.pow(p_slope,float(1)/2)
                    if V<0.15: #If the calculated V is smaller than 0.15m/s we assume a minimum velocity of 0.15m/s:
                        V=0.15
                t_adj[catch_muid.index(catch_id)].append(float(p_length)/V)
                pipe_adj[catch_muid.index(catch_id)].append(p_name)
                
    return(t_adj, pipe_adj)

def flowtime(current,branch,t_p_adj,t_adj,pipe_adj,v_p_data, names, slope, length, diameters, manning, node_s, node_e, level_lo, level_up, catch_muid, node_inlet, update_travelTime):
    # The function calculates the time it takes for water to flow between two points in the network branch
    # using the Manning formula for calculations of each pipe on the flowpath. Summarising the flow time 
    # of pipes leading to each cathment gives the time compensations to implement. 
    
    import math
    import pdb
    from MiscFunctions import RestoreNetworkConnections
    from MiscFunctions import CalculateVelocity
    
    # if travel time is not to be updated skip this function
    if update_travelTime==False:
        return(t_p_adj,t_adj,pipe_adj,v_p_data)
    
    # Part 1:
    # Calculate flow velocity and flow time for each pipe in the branch.
    for pipe in branch: 
        pindex=names.index(pipe)
        
        # Slope can be infinitely small hence velocity as well.
        # We therefore define threshold of too low slope where we use another approach.
        slope_tresh=0.0005 #(m/m)
        
        # if the slope is above or equal to 0 but lower than the threshold
        if slope[pindex]>=0 and slope[pindex]<=slope_tresh:
            
            # if this is the only pipe in the branch we assume the velocity to be 0.15 m/s based on figure on page 80 in Afloebsteknik. 
            if len(branch)==1:
                V=0.15 # manual estimation of 15 cm/s (page 80, Afloebsteknik)
                p_length=length[pindex]
                t_p_adj[pindex]=float(p_length)/V
                continue   
            
            # if multiple pipes in branch base the velocity on those.
            else:
                #if the last pipe in branch
                if pipe==branch[-1]:
                    V=0.15 #manual estimation
                    p_length=length[pindex]
                    t_p_adj[pindex]=float(p_length)/V
                    continue
                else:
                    [V, t_p_adj]=CalculateVelocity(pipe, branch, slope, slope_tresh, t_p_adj, names, node_s, node_e, length, diameters, manning, level_lo, level_up)
                    
        # if the slope is above the threshold calculate the velocity in a normal way. 
        else:
            p_length=length[pindex]
            p_dia=diameters[pindex]
            p_manning=manning[pindex]
            p_slope=slope[pindex]/float(100) # conversion to metre per meter from cm per metre
            hyd_rad=(math.pi*math.pow(p_dia/float(2),2))/(math.pi*p_dia) # hydraulic radius
            V=p_manning*math.pow(hyd_rad,float(2)/3)*math.pow(p_slope,float(1)/2) # Manning formula
            t_p_adj[pindex]=float(p_length)/V
            # If calculated velocity < 15cm/s use another (higher) slope from another pipe for calculation
            if V<0.15:
                # If only pipe in branch: manual estimation.
                if len(branch)==1:
                    V=0.15 # manual estimation of 15 cm/s (page 80, Afloebsteknik)
                    p_length=length[pindex]
                    t_p_adj[pindex]=float(p_length)/V
                    continue
                    
                else:
                    if pipe==branch[-1]:
                        V=0.15 #manual estimation
                        p_length=length[pindex]
                        t_p_adj[pindex]=float(p_length)/V
                        continue
                    else:
                        [V, t_p_adj]=CalculateVelocity(pipe, branch, slope, slope_tresh, t_p_adj, names, node_s, node_e, length, diameters, manning, level_lo, level_up)
        try:
            v_p_data[pindex]=V
        except:
            pdb.set_trace()
            print('No V defined')
            # investigate this
            # pdb.set_trace()
        
    ############################
    # Part 2: 
    # Assign time delay to each catchment based on the location of the inlet node in the branch
    for pipe in branch: 
        start_n=node_s[names.index(pipe)]
        end_n=node_e[names.index(pipe)]
        pdel_index=branch.index(pipe)
        catch_ids=list()
        # if pipe is connected to 'current' node:
        if start_n==current:
            # make list of connected catchments to the opposite node (there is no delay to the 'current' node)
            [catch_ids.append(catch_muid[l]) for l in range(len(catch_muid)) if node_inlet[l]==end_n]
            
            if len(branch)>1:
                lastpipe=False
                #if not the last pipe continue to next pipe and the common node will be used(same as the end in current pipe)
                continue
            else:
                # if there is only one pipe save the information and continue
                lastpipe=True
                for catch in catch_ids:
                    if t_p_adj[names.index(pipe)] not in t_adj[catch_muid.index(catch)]:
                        t_adj[catch_muid.index(catch)].append(t_p_adj[names.index(pipe)])
                    if pipe not in pipe_adj[catch_muid.index(catch)]:
                        pipe_adj[catch_muid.index(catch)].append(pipe)
                continue
        
        # just opposite of the previous hence the 'startnode' is the one to delay        
        elif end_n==current:
            # make list of connected catchments to the opposite node (there is no delay to the 'current' node)
            [catch_ids.append(catch_muid[l]) for l in range(len(catch_muid)) if node_inlet[l]==start_n]
            
            if len(branch)>1:
                lastpipe=False
                #if not the last pipe continue to next pipe and common node will be used(same as the end in current pipe)
                continue
            else:
                # if there is only one pipe save the information and continue
                lastpipe=True
                for catch in catch_ids:
                    if t_p_adj[names.index(pipe)] not in t_adj[catch_muid.index(catch)]:
                        t_adj[catch_muid.index(catch)].append(t_p_adj[names.index(pipe)])
                    if pipe not in pipe_adj[catch_muid.index(catch)]:
                        pipe_adj[catch_muid.index(catch)].append(pipe)
                continue
                        
        
        # For rest of the branch:
        elif end_n!=current and start_n!=current:    
            
            #Nodes connected to previous pipe:
            start_b=node_s[names.index(branch[pdel_index-1])]
            end_b=node_e[names.index(branch[pdel_index-1])]
            
            # if pipe is not connected but "a wrong turn" take the 2nd previous pipe (which is hopefully connected to this one):
            if start_b != start_n and end_b != start_n and start_b != end_n and end_b != end_n:
                #lastpipe=True
                flag=5
            
            #if first pipe in branch:
            if pdel_index == 0:
                print('check this condition: when/how does this happen? first element in branch should be connected to "current"') # when/how does this happen? first element in branch should be connected to 'current'
                pdb.set_trace()
                flag=3
                #next pipe:
                if pdel_index >= pdel_index+1:
                    start_b=node_s[names.index(branch[pdel_index+1])]
                    end_b=node_e[names.index(branch[pdel_index+1])]

            #next pipe:
            if len(branch)-1>= pdel_index+1: #only assign these if not last pipe in branch list
                start_a=node_s[names.index(branch[pdel_index+1])]
                end_a=node_e[names.index(branch[pdel_index+1])]
                lastpipe=False
            else:
                #last pipe in del_pipes
                start_a=None
                end_a=None
                lastpipe=True
            
            # find common node:
            if start_n==start_b or start_n==end_b:
                [catch_ids.append(catch_muid[l]) for l in range(len(catch_muid)) if node_inlet[l]==start_n]
                n_s=start_n
                n_e=end_n
                flag=1
            
            elif end_n==start_b or end_n==end_b:
                [catch_ids.append(catch_muid[l]) for l in range(len(catch_muid)) if node_inlet[l]==end_n]
                n_s=end_n
                n_e=start_n
                flag=2
            
            #check if this is the last pipe in branch:
            if not start_n==start_a and not start_n==end_a and not end_n==start_a and not end_n==end_a:
                lastpipe==True
            
            #if this pipe is connected to the next ignore this pipe as it will be calculated next time.
            
            if lastpipe==False and flag==3:
                continue
                
            ########### insert flow time differences ################
            flowpath=branch[0:branch.index(pipe)] #+1 to include current pipe
            flowpath.reverse()
            
            
            #calculate travel time for common node
            if lastpipe==False:
                for catch in catch_ids:
                    for pipe_sec in flowpath:
                        if t_p_adj[names.index(pipe_sec)] not in t_adj[catch_muid.index(catch)]:
                            t_adj[catch_muid.index(catch)].append(t_p_adj[names.index(pipe_sec)])
                        if pipe_sec not in pipe_adj[catch_muid.index(catch)]:
                            pipe_adj[catch_muid.index(catch)].append(pipe_sec)
                        
            #also calculate travel time for end node
            if lastpipe==True: 
                
                #first for downstream node:
                for catch in catch_ids:
                    for pipe_sec in flowpath:
                        if t_p_adj[names.index(pipe_sec)] not in t_adj[catch_muid.index(catch)]:
                            t_adj[catch_muid.index(catch)].append(t_p_adj[names.index(pipe_sec)])
                        if pipe_sec not in pipe_adj[catch_muid.index(catch)]:
                            pipe_adj[catch_muid.index(catch)].append(pipe_sec) 
                # then for upstream node
                catch_ids=list()
                if flag==1:
                    [catch_ids.append(catch_muid[l]) for l in range(len(catch_muid)) if node_inlet[l]==end_n]
                                        
                elif flag==2:
                    [catch_ids.append(catch_muid[l]) for l in range(len(catch_muid)) if node_inlet[l]==start_n]
                
                #new flowpath including end pipe
                flowpath=branch[0:branch.index(pipe)+1] #+1 to include current pipe
                flowpath.reverse()
                
                # save information to lists
                for catch in catch_ids:
                    for pipe_sec in flowpath:
                        if t_p_adj[names.index(pipe_sec)] not in t_adj[catch_muid.index(catch)]:
                            t_adj[catch_muid.index(catch)].append(t_p_adj[names.index(pipe_sec)])
                        if pipe_sec not in pipe_adj[catch_muid.index(catch)]:
                            pipe_adj[catch_muid.index(catch)].append(pipe_sec)
                            
    return(t_p_adj,t_adj,pipe_adj,v_p_data)
    