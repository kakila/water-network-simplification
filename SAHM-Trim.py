"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
### Main script for the trimming approach in SAHM: Cuts branches that only contain small pipe segments of limited diameter and length out of a network from a MIKE URBAN geodatabase (.gdb)

print('Initializing...')
import sys, os
import pdb
import sys
import shutil

#import arcpy
sys.path.append(r'C:\Program Files (x86)\ArcGIS\Desktop10.1\arcpy')
sys.path.append(r'C:\Program Files (x86)\ArcGIS\Desktop10.1\bin')

import arcpy
import numpy as np
import math
import time
from time import strftime
import pickle

#other defined tools
from CleaningUtils import *
from UpdateCoupleFile import UpdateCoupleFile
#from NewCoupleFile import *
from MakeNetworkFile import *
from LoadData import *
from MiscFunctions import *
from flowtime import *
from checkbranches import *
from goUpstream import *
from compensations import *
from updateBoundaries import *
from SaveData import *
from NetworkConnectivity import NetConnectivity
print('Script initiated at %s' % strftime("%Y-%m-%d %H:%M:%S"))
#from osgeo import ogr
#################################################################
#append script directory to PATH variable
wd=os.path.dirname(os.path.realpath(__file__))
sys.path.append(wd)

#when calling function in function a default recursion depth is set at 1000 in order to prevent crashing. sys.setrecusionlimit(x) let you specify a new limit.
sys.setrecursionlimit(20000)

#######################################################
######################## Input ########################
#######################################################       
# Thresholds for diameter and length:

# Main threshold:
# pipes with diameters larger than this will not be deleted
diathreshmain=0.45       #[m]

# Secondary threshold [m]:
# pipes larger than this diameter and branch length will not be deleted 
# as they may influence the system too much.
diathreshsecond=0.35     # [m]
treshlengthsecond=100   # [m]

#add folder attribute:
att='Trim'+str(int(diathreshmain*1000))

# Generate network connectivity-file (or load a previously generated one):
GenerateNetworkConnectivity=True

######### MODULES #########
#To exclude compensation set the following to False:
includeCompensations=True

# Make compensations for travel time and volume losses.
if includeCompensations:
    update_travelTime=True
    update_boundaries=True
else:
    update_travelTime=False
    update_boundaries=False


#set path to database
pathnetconn=os.path.abspath(r'C:\Models\Databases\Original\PipeConnectionFiles\linknode_full.txt')  # Network connectivity of parent model (if already generated, otherwise generate by setting GenerateNetworkConnectivity = True)
pathdb=os.path.abspath(r'C:\Models\Databases\Original\SimulationServerAustralia.gdb')               # Original MIKE URBAN Geodatabse (Parent model)
pathcouple=os.path.abspath(r'C:\Models\Databases\Original\1D2D_NetworkDANCE.couple')                # Original MIKE Flood couple-file
pathnewdb=os.path.abspath(r'C:\Models\Databases\Trim' + os.sep + att + os.sep + 'SimulationServerAustralia.gdb')    # New database with simplified model
pathnewcouple=os.path.abspath(r'C:\Models\Databases\Trim' + os.sep + att + os.sep + '1D2D_NetworkDANCE.couple')     # New MIKE Flood couple-file

#specify path to save generated selection files:
path_newselct=os.path.abspath(r'C:\Models\Databases\Trim' + os.sep + att + os.sep + 'SelectionFiles')   # Files for assigning catchment rain series.
path_txt=os.path.abspath(r'C:\Models\Databases\Trim' + os.sep + att + os.sep + 'Textfiles')             # Directory for txt-files containing raw data for e.g. volume compensations and travel-time.

########## GENERATE NETWORK CONNECTIVITY ############
# only needed once for the full network as output is saved to txt-files
if GenerateNetworkConnectivity:
    # set threshold distance between coordinates in metres
    threshdist= 0.3 # [m]

    # output path for the connectivity-file
    #...\<PARENT MODEL FOLDER>\ConnectivityFile\NetworkConnectivity 
    txtfile=r'.\ConnectivityFiles\NetworkConnectivity'

    #run function
    pipelist=NetConnectivity(pathdb, threshdist, txtfile)
    pathnetconn=txtfile+'.txt'

################# LOAD DATA #########################

[geom, rows, FID_p, names, diameters, heights, widths, length, length_c, slope, manning, level_lo, level_up, crs, pipecoor_s, pipecoor_e, t_p_adj, v_p_data, c_list, pipe_adj, all_names, all_diameters, all_length, all_slope, 
new_uplevel, new_lolevel, new_polylines, new_dia, new_height, new_width, new_length, del_rows, 
FID_n, all_FID_n, row_nodes, names_n, all_names_n, nodecoor, n_diameters, n_inlev, n_ground, outlets,
FID_i, catch_muid, node_inlet, all_node_inlet, t_adj, newconnodes, oldconnodes, FID_i_link, catch_line, node_s, node_e, all_node_s, all_node_e, s_node_pipes, e_node_pipes, s_node_pipes_full, e_node_pipes_full,
list1]=LoadData(pathdb, pathcouple, pathnetconn, pathnewdb, pathnewcouple, path_newselct, path_txt, False, GenerateNetworkConnectivity)


########################################################################################
########################### MAIN SCRIPT ################################################
########################################################################################    
print('Main script started')
#Check if startnodes is in nodes:

for row in outlets:
    if row not in names_n:
        print('startnode %s not in nodes' %row)

#set start point for search
node_inlet_new=list()
for startpoint in outlets: 
    if startpoint==outlets[0]:
        [pipes_del,origin,visited,t_p_adj,t_adj,pipe_adj,v_p_data, s_node_pipes, e_node_pipes, node_inlet_new, catch_line]=go_upstream(startpoint,[],[],[],t_p_adj,t_adj,pipe_adj,v_p_data,0, s_node_pipes, e_node_pipes, names, diameters, length, slope, manning, node_s, node_e, level_up, level_lo, catch_muid, node_inlet, update_travelTime, FID_i, all_names_n, catch_line, s_node_pipes_full, e_node_pipes_full, nodecoor, diathreshmain, diathreshsecond, treshlengthsecond)
        
    else:
        [pipes_del,origin,visited,t_p_adj,t_adj,pipe_adj,v_p_data, s_node_pipes, e_node_pipes, node_inlet_new, catch_line]=go_upstream(startpoint,pipes_del,origin,visited,t_p_adj,t_adj,pipe_adj,v_p_data,0, s_node_pipes, e_node_pipes, names, diameters, length, slope, manning, node_s, node_e, level_up, level_lo, catch_muid, node_inlet_new, update_travelTime, FID_i, all_names_n, catch_line, s_node_pipes_full, e_node_pipes_full, nodecoor, diathreshmain, diathreshsecond, treshlengthsecond)

################ COMPENSATIONS #################
        
# Travel time:
(catch_time_data, catch_tlist)=TravelTime(t_adj,node_inlet_new, FID_i, catch_muid, oldconnodes, path_txt, False, [])

# Makes .mus selection files of which catchments uses what rain input:
[name_list, rain_list]=MakeRainSelectionLists(catch_tlist,catch_time_data, catch_muid, path_newselct)

# Calculate volume losses/compensations and new diameters for manholes:

[inlet_node_unique, new_dia_n]=CalculateVolumeLoss(pipe_adj, v_p_data, True, names, node_s, node_e, all_node_s, all_node_e, length, diameters, slope, FID_i, catch_muid, node_inlet_new, all_node_inlet, pipes_del, names_n, n_diameters, n_ground, n_inlev, path_txt, 'trim')

print('Main script finished succesfully') 

############### SAVE DATABASE ###############

# Update boundary conditions
updateBoundary(name_list, rain_list)

# Save new geodatabase for MIKE URBAN 
(rest_nodes, names_red, node_s_red, node_e_red)=SaveData(update_travelTime, True, update_boundaries, pathnewdb, pipes_del, names, node_s, node_e, level_up, level_lo, pipecoor_s, pipecoor_e, FID_n, names_n, inlet_node_unique, new_dia_n, FID_i, node_inlet_new, catch_line, all_FID_n, all_names_n)

# Create new .couple file for MIKE FLOOD 

print('Writing new .couple file' )
UpdateCoupleFile(rest_nodes,pathcouple,pathnewcouple,False)
print('Couple-file updated')

print('Trim complete')

# Save network connectivity.
MakeNetworkFile(names_red,node_s_red,node_e_red,path_txt,att)

print('Trim script finished succesfully at %s' % strftime("%Y-%m-%d %H:%M:%S"))
