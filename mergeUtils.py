"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# Update feature coordinates, catchment assignments and calculate new link data when links are merged.

def update_line(linestr,location,flag):
    #modify start (flag==0) or end (flag==1) point of linestr according to location
    #(create new line with points taken from the original line and the new start/end point)
    import arcpy
    
    if flag==0:
        for part in linestr:
            part.insert(0,location)
            newline=arcpy.Polyline(part) #convert new array to polyline
       
    elif flag==1:
        for part in linestr:
            part.append(location)
            newline=arcpy.Polyline(part) #convert new array to polyline
  
    return newline
    
def change_catchment_assignments(node_del, pipe_del, pipe_merge, node_inlet, names, all_names_n, level_up, level_lo, node_s, node_e, FID_i, CatchAddLength, catch_line, newconnodes, length_c, slope, diameters, manning, catch_muid, nodecoor):
    #Reconnect catchment to the node which is not deleted
    import arcpy
    #list of inlets where node_id needs to be changed
    index=[x for x in range(len(node_inlet)) if node_inlet[x]==node_del]
    reassign=[FID_i[x] for x in index]
    
    #start and end node of the pipe to merge with
    ns=node_s[names.index(pipe_merge)]
    ne=node_e[names.index(pipe_merge)]
    
    mulev=level_up[names.index(pipe_merge)]
    mllev=level_lo[names.index(pipe_merge)]
    del_uplev=level_up[names.index(pipe_del)]
    del_lolev=level_lo[names.index(pipe_del)]
    #reassign catchment to that node of the pipe to merge with, which is not deleted
    
    # Check elevations of links to locate which node is downstream (catchments and compensations will be assigned to this node). 
    if mulev+mllev >= del_uplev+del_lolev:
        ns_del=node_s[names.index(pipe_del)]
        ne_del=node_e[names.index(pipe_del)]    
        if ns_del==node_del:
            newnode=ne_del
        elif ne_del==node_del:
            newnode=ns_del
        for ind in index:
            # data for the time compensation
            CatchAddLength[ind].append([length_c[names.index(pipe_del)],slope[names.index(pipe_del)],diameters[names.index(pipe_del)],manning[names.index(pipe_del)],catch_muid[ind],pipe_del])

    if mulev+mllev < del_uplev+del_lolev:
        if ns==node_del:
            newnode=ne
        elif ne==node_del:
            newnode=ns
        for ind in index:
            CatchAddLength[ind].append([length_c[names.index(pipe_merge)],slope[names.index(pipe_merge)],diameters[names.index(pipe_merge)],manning[names.index(pipe_merge)],catch_muid[ind],pipe_merge])
    
    # define the new inlet node, coordinate, and polyline for the catchments connected to the deleted node.        
    for fidi in reassign:
        ind_fidi=FID_i.index(fidi)
        newconnodes[ind_fidi]=newnode
        

        newpoint=nodecoor[all_names_n.index(newnode)]
        newarray=arcpy.Array([catch_line[ind_fidi].firstPoint,newpoint])
        catch_line[ind_fidi]=arcpy.Polyline(newarray)
                    
    for i in index:
        node_inlet[i]=newnode
    return(node_inlet, catch_line, newconnodes, CatchAddLength)
        
def update_connections(node_del,pipe_del,pipe_merge, names, all_names, node_s, node_e, pipecoor_s, pipecoor_e, level_up, level_lo, s_node_pipes, e_node_pipes, names_n, nodesdel, node_inlet, FID_n, new_uplevel, new_lolevel):
    #deletes node_del from the node feature class and the dictionaries
    #reconnects the pipe to merge to the non-common node of the pipe to remove
    #resets the corresponding level of the merged pipe in search lists and feature class
    #reconnects all other pipes connected to node_del to the next downstream node and adjusts level if the pipe level is lower than that of the node
       
    #update the connection of pipe_merge (go to node of pipe_del) - in feature class and search lists, extract also location of the nodes for the merged pipe
    ind_pdel=names.index(pipe_del)
    ind_pdel_all=all_names.index(pipe_del)
    ind_pmerge=names.index(pipe_merge)
    ind_pmerge_all=all_names.index(pipe_merge)
        
    #find new end node for merged pipe
    if node_del == node_s[ind_pdel]: 
        n_replace = node_e[ind_pdel]

        loc_replace=pipecoor_e[ind_pdel_all] 
        l_replace = level_lo[ind_pdel]
    elif node_del == node_e[ind_pdel]: 
        n_replace = node_s[ind_pdel]
        loc_replace=pipecoor_s[ind_pdel_all]
        l_replace = level_up[ind_pdel]
    
    #update merged pipe connection information
    if node_del == node_s[ind_pmerge]: 
        n_not_replace = node_e[ind_pmerge]
        loc_not_replace=pipecoor_e[ind_pmerge_all]
        node_s[ind_pmerge] = n_replace
        if n_replace in s_node_pipes:
            if not pipe_merge in s_node_pipes[n_replace]: s_node_pipes[n_replace].append(pipe_merge)
        else:
            s_node_pipes[n_replace]=[pipe_merge]
        l_origin = level_lo[ind_pmerge] #if start node is moved, then downstream level remains unchanged
        level_up[ind_pmerge]=l_replace #replace the other pipe level
        new_uplevel[ind_pmerge_all]=l_replace
        node_s[ind_pmerge]=n_replace
        nodesdel.append(node_del)

    elif node_del == node_e[ind_pmerge]: 
        n_not_replace = node_s[ind_pmerge]
        loc_not_replace = pipecoor_s[ind_pmerge_all]
        node_e[ind_pmerge] = n_replace
        if n_replace in e_node_pipes:
            if not pipe_merge in e_node_pipes[n_replace]: e_node_pipes[n_replace].append(pipe_merge)
        else:
            e_node_pipes[n_replace]=[pipe_merge]
        l_origin = level_up[ind_pmerge]
        level_lo[ind_pmerge]=l_replace
        new_lolevel[ind_pmerge_all]=l_replace
        node_e[ind_pmerge]=n_replace
        nodesdel.append(node_del)

    #find flow direction of the resulting merged pipe from levels of pipe_del, pipe_merge
    if l_origin<l_replace: 
        node_reconnect=n_not_replace
        loc_reconnect=loc_not_replace
        level_reconnect=l_origin
    else: 
        node_reconnect=n_replace
        loc_reconnect=loc_replace
        level_reconnect=l_replace   
    
    #delete entry for the deleted node in dictionaries
    if node_del in s_node_pipes: del s_node_pipes[node_del]
    if node_del in e_node_pipes: del e_node_pipes[node_del]
    if n_replace in s_node_pipes:
        if pipe_del in s_node_pipes[n_replace]: s_node_pipes[n_replace]=[p for p in s_node_pipes[n_replace] if not p==pipe_del]
    if n_not_replace in s_node_pipes:
        if pipe_del in s_node_pipes[n_not_replace]: s_node_pipes[n_not_replace]=[p for p in s_node_pipes[n_not_replace] if not p==pipe_del]
    if n_replace in e_node_pipes:
        if pipe_del in e_node_pipes[n_replace]: e_node_pipes[n_replace]=[p for p in e_node_pipes[n_replace] if not p==pipe_del]
    if n_not_replace in e_node_pipes:
        if pipe_del in e_node_pipes[n_not_replace]: e_node_pipes[n_not_replace]=[p for p in e_node_pipes[n_not_replace] if not p==pipe_del]
    #delete entry in node lists
    del FID_n[names_n.index(node_del)], names_n[names_n.index(node_del)]
    #replace node_del by node_reconnect in the inlet list
    indices = [i for i, x in enumerate(node_inlet) if x == node_del]
    
    for i in indices:
        node_inlet[i]=node_reconnect
        
    return(node_inlet, s_node_pipes, e_node_pipes, nodesdel, FID_n, names_n)
    
def merge_pipes(pipe_del,pipe_merge, FID_p, names, all_names, geom, rows, node_s, node_e, crs, diameters, length_c, heights, widths, v_p_data, new_polylines, new_dia, new_length, new_height, new_width, del_rows, level_up, level_lo, slope, manning, vol_diff):
    #update pipe geometry of merge pipe and reduce search lists, levels were already updated in updated connections
    import math
    import pdb
    
    ind_pdel=names.index(pipe_del)
    ind_pdel_all=all_names.index(pipe_del)
    ind_pmerge=names.index(pipe_merge)
    ind_pmerge_all=all_names.index(pipe_merge)    
    
    line=geom[ind_pmerge_all]
    line_orig=rows[ind_pmerge_all]
    
    line_del=geom[ind_pdel_all]
    line_del_orig=rows[ind_pdel_all]
    
    #for the merged pipe, the search list for upstream nodes was already updated in "update_connections".
    if node_s[ind_pmerge] == node_s[ind_pdel]: #start point of new pipe must equal start point of pipe to delete
        loc=line_del.firstPoint
        #nloc=line_del.lastPoint
        line=update_line(line,loc,0)
    elif node_s[ind_pmerge] == node_e[ind_pdel]: #start point of new pipe must equal end point of pipe to delete
        loc=line_del.lastPoint
        #nloc=line_del.firstPoint
        line=update_line(line,loc,0)
    elif node_e[ind_pmerge] == node_s[ind_pdel]: #end point of new pipe must equal start point of pipe to delete
        loc=line_del.firstPoint
        #nloc=line_del.lastPoint
        line=update_line(line,loc,1)
    elif node_e[ind_pmerge] == node_e[ind_pdel]: #end point of new pipe must equal end point of pipe to delete
        loc=line_del.lastPoint
        #nloc=line_del.firstPoint
        line=update_line(line,loc,1)

    # volume of original pipes:
    
    delp_volume=math.pi*math.pow((diameters[ind_pdel]/float(2)),2)*length_c[ind_pdel]
    mergep_volume=math.pi*math.pow((diameters[ind_pmerge]/float(2)),2)*length_c[ind_pmerge]
    
    #recompute a length averaged pipe diameter for the merged pipe
    if crs[ind_pmerge]==1: # 1=circular
        diameters[ind_pmerge]=(diameters[ind_pmerge]*length_c[ind_pmerge]+diameters[ind_pdel]*length_c[ind_pdel])/(length_c[ind_pmerge]+length_c[ind_pdel])
    elif crs[ind_pmerge]==3: #3=rectangular
        diameters[ind_pmerge]=(diameters[ind_pmerge]*length_c[ind_pmerge]+diameters[ind_pdel]*length_c[ind_pdel])/(length_c[ind_pmerge]+length_c[ind_pdel])
        heights[ind_pmerge]=(heights[ind_pmerge]*length_c[ind_pmerge]+heights[ind_pdel]*length_c[ind_pdel])/(length_c[ind_pmerge]+length_c[ind_pdel])
        widths[ind_pmerge]=(widths[ind_pmerge]*length_c[ind_pmerge]+widths[ind_pdel]*length_c[ind_pdel])/(length_c[ind_pmerge]+length_c[ind_pdel])
    else:
        print "merging - no average diameter computed" 
    
    #Volume difference:
    merge_volume_tmp=math.pi*math.pow((diameters[ind_pmerge]/float(2)),2) 
    
    # write volume differences to pipe index
    diff_delp=delp_volume-merge_volume_tmp*length_c[ind_pdel]
    diff_mergep=mergep_volume-merge_volume_tmp*length_c[ind_pmerge]
    #total difference (may be negative)
    v_p_data[ind_pdel_all].append(0)
    v_p_data[ind_pmerge_all].append(diff_delp+diff_mergep)
       
    #update length of the merged pipe
    length_c[ind_pmerge]=length_c[ind_pmerge]+length_c[ind_pdel]
    
    # data for UpdateCursor:
    geom[ind_pmerge_all]=line #update coordinate for pipes
    ind_pmerge_all=all_names.index(pipe_merge)
    # new link data: 
    new_polylines[ind_pmerge_all]=line
    new_dia[ind_pmerge_all]=diameters[ind_pmerge]
    new_height[ind_pmerge_all]=heights[ind_pmerge]
    new_width[ind_pmerge_all]=widths[ind_pmerge]
    new_length[ind_pmerge_all]=length_c[ind_pmerge]
    del_rows.append(line_del_orig)
    
    #delete pipe entries from all lists
    del FID_p[ind_pdel],names[ind_pdel],node_s[ind_pdel],node_e[ind_pdel],diameters[ind_pdel],heights[ind_pdel],widths[ind_pdel],length_c[ind_pdel],level_lo[ind_pdel],level_up[ind_pdel],crs[ind_pdel],slope[ind_pdel],manning[ind_pdel],vol_diff[ind_pdel]
    return(FID_p, names, geom, rows, node_s, node_e, crs, diameters, length_c, heights, widths, v_p_data, new_polylines, new_dia, new_length, new_height, new_width, del_rows, level_up, level_lo, slope, manning, vol_diff)