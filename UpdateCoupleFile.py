"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
###Updates MIKE FLOOD .Couple file after modifying nodes and links in database.

def UpdateCoupleFile(nodes,pathcouple,pathnewcouple,promt):
    from time import strftime
    #Insert the first few lines in couple file
    print('Updating .couple file.')
    couple = [couple1.rstrip('\n') for couple1 in open(pathcouple)]
    
    newcouple=couple[0:36]
    newcouple[0]='// Created     : %s' % strftime("%Y-%m-%d %H:%M:%S") #YYYY-MM-D(d) hh:mm:ss
    counternot=0
     
    for node in nodes:
        string="      Urban_ID = '%s'" %node
        try:
            index=couple.index(string)
            upindex=index-6
            dwindex=index+30 #includes empty line to be deleted
            getnode=couple[upindex:dwindex+1]
            for element in getnode:
                newcouple.append(element)
        except:
            if promt:
                print('Node %s not in couple-file' % node)
            counternot=counternot+1
            
    print('Total of %s nodes not in couple file' % counternot)
    
    # Update the number of items in the couple file
    NoCB='   Number_of_CoupledBoundaries = %s' % newcouple.count('   [Definition]')      
    newcouple[34]=NoCB
    #Number_of_CoupledBoundaries = 12928        
    
    #Put in the last section of couple file
    indexfin=couple.index('      [MFADcomponents]')
    indexend=couple.index('EndSect  // M11M21_Coupling_Parameters')+1 #indexend=len(couple)+1
    getend=couple[indexfin:indexend]
    for element in getend:
        newcouple.append(element)
    
    #pdb.set_trace()
    with open(pathnewcouple, 'w') as f:
        for s in newcouple:
            f.write(s) #+ 'n').encode('unicode-escape'))
            f.write("\n")
    
    return newcouple

###########################################
## UNCOMMENT BELOW FOR STANDALONE SCRIPT ##
###########################################
# print('Initializing...')
# import sys, os

# import sys
# from CleaningUtils import *
# import arcpy
# import shutil
# import numpy as np
# import pickle
# from time import strftime

# #################################################################
# #append script directory to PATH variable
# wd=os.path.dirname(os.path.realpath(__file__))
# sys.path.append(wd)

# sys.path.append(r'C:\Program Files (x86)\ArcGIS\Desktop10.1\arcpy')
# sys.path.append(r'c:\Program Files (x86)\ArcGIS\Desktop10.1\bin')


# #set path to database
# pathdb=os.path.abspath(r'D:\Models\tmp\SimulationServermerge.gdb')
# pathcouple=os.path.abspath(r'D:\Models\tmp\1D2D_NetworkDANCE.couple')
# pathnewcouple=os.path.abspath(r'D:\Models\tmp\1D2D_NetworkDANCE_new.couple')

# pathnodes="in_memory/nodes"
# arcpy.CopyFeatures_management(pathdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Node',pathnodes)

# nodes=[[] for i in range(0)]
# fields_node = ['MUID']
# with arcpy.da.SearchCursor(pathnodes,fields_node) as cursorno:
    # for row in cursorno:
        # nodes.append(str_robust(row[0]))


# with open(pathcouple, 'r') as couple_file:
    # couple=couple_file.readlines()
#first line of coupled nodes: index 36: '   [Definition]'
#Node ID listed in index 42:            "      Urban_ID = '1'"
#last line of coupled node: index 72:   '   EndSect  // Definition'
  
# UpdateCoupleFile(nodes,pathcouple,pathnewcouple)
# print ('Couple file updated!')
###########################################  