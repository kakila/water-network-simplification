"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# Functions for checking conditions before merging two links.

def mergeCheck(merge_pipe, name, names, crs, diameters, s_node_pipes, e_node_pipes, node_s, node_e, thresh_diam, diam_del):
    #check for "do not merge criteria"
    
    from CleaningUtils import check_parallel
    
    check_del=0
    #do not merge special cross sections
    #if crs[names.index(name)] in ['RECT_CLOSED','W','IRREGULAR'] or crs[names.index(merge_pipe)] in ['RECT_CLOSED','W','IRREGULAR']: check_del=1
    if crs[names.index(name)] in [2] or crs[names.index(merge_pipe)] in [2]: check_del=1
    #do not merge too big diameter differences
    elif abs(diam_del-diameters[names.index(merge_pipe)])>diam_del*thresh_diam: check_del=1
    #do not merge if there is a parallel pipe - check if there is another pipe, that is connected both to start and end node
    test=check_parallel(name,s_node_pipes,e_node_pipes,node_s[names.index(name)],node_e[names.index(name)])
    if test: check_del=1 #if there is more than 0 other pipes connected to start and end point ->do not merge
    return(check_del)
    
def merge(minlength, thresh_diam, geom, rows, FID_p, names, diameters, heights, widths, length, length_c, slope, manning, level_lo, level_up, crs, pipecoor_s, pipecoor_e, vol_diff, v_p_data, CatchAddLength, pipe_adj, all_names, all_diameters, all_length, all_slope, new_uplevel, new_lolevel, new_polylines, new_dia, new_height, new_width, new_length, del_rows, FID_n, all_FID_n, row_nodes, names_n, all_names_n, nodecoor, n_diameters, n_inlev, n_ground, FID_i, catch_muid, node_inlet, t_adj, newconnodes, oldconnodes, FID_i_link, catch_line, node_s, node_e, s_node_pipes, e_node_pipes, s_node_pipes_full, e_node_pipes_full, nodesdel):
    from CleaningUtils import *
    from mergeUtils import *
    import time
    from time import strftime
    import pdb    
    
    print('Main script started at %s ' % time.strftime("%H:%M:%S"))
    
    # Creates list of lengths to iterate through when merging
    thresh_length=[5]*4
    if minlength<=25:
        for i in range(10,minlength,5)+[minlength]:
            thresh_length.extend([i]*4)

    elif 25<minlength and minlength<=50:
        for i in range(10,25+1,5):
            thresh_length.extend([i]*4)
        for j in range(30,minlength,10)+[minlength]:
            thresh_length.extend([j]*5)
            
    elif 50<minlength and minlength<=100:
        for i in range(10,25+1,5):
            thresh_length.extend([i]*4)
        for j in range(30,50+1,10):
            thresh_length.extend([j]*5)
        for k in range(50,minlength,20)+[minlength]:
            thresh_length.extend([k]*5)

    elif 100<minlength and minlength<=200:
        for i in range(10,25+1,5):
            thresh_length.extend([i]*4)
        for j in range(30,50+1,10):
            thresh_length.extend([j]*5)
        for k in range(50,100+1,25):
            thresh_length.extend([k]*5)    
        for l in range(100,minlength,30)+[minlength]:
            thresh_length.extend([l]*5)
    
    # Begin main merge process:
    for thresh in thresh_length: #start deleting very short pipes first, then go to longer ones
        counter=0
        while counter< len(names):
            if counter % 500 ==0:
                if counter == 0:
                    print("Merging pipes shorter than %s meter and maximum of %s%%  diameter difference " % (thresh, thresh_diam*100))
                else:    
                    print("counter %s of %s" % (counter, len(names)))
            #print counter
            #check length, do only if length is below threshold
            if length_c[counter]<thresh:
                del_node=None
                name=names[counter]
                diam_del=diameters[counter]
                #find pipes connected to the concerned pipe
                n_start=node_s[counter]
                n_end=node_e[counter]
                #pipes connected to the starting node
                if n_start in s_node_pipes: p_start=[p for p in s_node_pipes[n_start]]
                else: p_start=[]
                if n_start in e_node_pipes: p_start=p_start+e_node_pipes[n_start]
                #pipes connected to the starting node
                if n_end in e_node_pipes: p_end=[p for p in e_node_pipes[n_end]]
                else: p_end=[]
                if n_end in s_node_pipes: p_end=p_end + s_node_pipes[n_end]
                
                #remove considered pipe from the list of pipes
                if name in p_start: del p_start[p_start.index(name)]
                if name in p_end: del p_end[p_end.index(name)]
                #if a pipe shows up in both the start and the end point, we can not merge it here (parallel pipe) --> this should be cleaned separately --> stop merging (otherwise, there is a risk of destroying paths by deleting one of the connecting nodes (if one of the parallel pipes is merged with a 3rd pipe))
                flag=0
                if not p_start==[]: 
                    for p in p_start:
                        if p in p_end: flag=1
                if flag==1: counter=counter+1; continue
                ##figure out flow direction from levels of considered pipe

                #different action for different numbers of connected pipes
                if len(p_start)==1 and len(p_end)==1: #one connection on each side - merge with shorter one
                    if length_c[names.index(p_start[0])] < length_c[names.index(p_end[0])]:
                        del_node=n_start
                        merge_pipe=p_start[0]
                        other_pipes=[]
                    else:
                        del_node=n_end
                        merge_pipe=p_end[0]
                        other_pipes=[]
                elif len(p_start)==1:
                    del_node=n_start
                    merge_pipe=p_start[0]
                    other_pipes=[]
                elif len(p_end)==1:
                    del_node=n_end
                    merge_pipe=p_end[0]
                    other_pipes=[]
                elif len(p_start)>1 and len(p_end)==0:
                    #do not merge, quite high probability of hitting a main branch
                    counter=counter+1
                    continue
                elif len(p_end)>1 and len(p_start)==0:
                    #do not merge, quite high probability of hitting a main branch
                    counter=counter+1
                    continue
                elif len(p_end)>1 and len(p_start)>1:
                    # intersection between 3 links or more
                    counter=counter+1
                    continue                
                else: #there are no other pipes connected
                    counter=counter+1
                    continue
                check_del=mergeCheck(merge_pipe, name, names, crs, diameters, s_node_pipes, e_node_pipes, node_s, node_e, thresh_diam, diam_del)
                               # if test: check_del=1 #if there is more than 0 other pipes connected to start and end point ->do not merge
                if check_del==0:
                    (node_inlet, catch_line, newconnodes, CatchAddLength)=change_catchment_assignments(del_node,name,merge_pipe,node_inlet, names, all_names_n, level_up, level_lo, node_s, node_e, FID_i, CatchAddLength, catch_line, newconnodes, length_c, slope, diameters, manning, catch_muid, nodecoor) 
                    (node_inlet, s_node_pipes, e_node_pipes, nodesdel, FID_n, names_n)=update_connections(del_node,name,merge_pipe, names, all_names, node_s, node_e, pipecoor_s, pipecoor_e, level_up, level_lo, s_node_pipes, e_node_pipes, names_n, nodesdel, node_inlet, FID_n, new_uplevel, new_lolevel)
                    #update_connections(del_node,name,merge_pipe) 
                    (FID_p, names, geom, rows, node_s, node_e, crs, diameters, length_c, heights, widths, v_p_data, new_polylines, new_dia, new_length, new_height, new_width, del_rows, level_up, level_lo, slope, manning, vol_diff
                    )=merge_pipes(name,merge_pipe, FID_p, names, all_names, geom, rows, node_s, node_e, crs, diameters, length_c, heights, widths, v_p_data, new_polylines, new_dia, new_length, new_height, new_width, del_rows, level_up, level_lo, slope, manning, vol_diff)
                    #merge_pipes(name,merge_pipe)
                    
                    continue #if we merged out a pipe->do not increase index
                
            counter=counter+1 #increase index if list was not modified
    
    # Create list with pipes to be deleted.
    pipe_del=list()
    for row in del_rows:
        pipe_del.append(row[1])
    
    return(CatchAddLength, t_adj, pipe_adj, v_p_data, node_inlet, pipe_del, node_s, node_e, names_n)
    
    