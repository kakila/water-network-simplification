"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# Functions for determiniation and application of time and volume compensations.
def TravelTime(t_adj,node_inlet, FID_i, catch_muid, oldconnodes, path_txt, TrimMerge, txtpath): 
    #Calculates travel time for catchment inlets that have been moved.
    import math
    import sys, os
    import pickle
    
    print('Calculating travel time')
     
    #save flow time to text files:

    # list of catchments with time delay and inlet node
    catch_time_data=list()
    catch_tlist=list()
    
    # Load data from previosly trimmed model. 
    if TrimMerge==True:
        with open(txtpath, 'rb') as f:
            trimtime = pickle.load(f)  #  0:FID   1:MUID   2:old Inlet node  3:new Inlet node    4:tot_t delay    5:Time delay
        for catch in trimtime:
            t_adj[catch_muid.index(catch[1])].append(catch[4])
        
    # Summarize flow times for each catchment
    for i in range(len(FID_i)):
        if t_adj[i]:
            try:
                tot_time=math.fsum(t_adj[i])
            except:
                tot_time=0
            #                        FID          MUID       old Inlet node  new Inlet node tot_t delay  Time delay
            catch_time_data.append([FID_i[i], catch_muid[i], oldconnodes[i], node_inlet[i], tot_time, t_adj[i]])
            catch_tlist.append(catch_muid[i])
            
    # Save data to file
    with open(path_txt+os.sep+'CatchTimeDataTrimfunc.txt', 'w') as f:
        f.write("FID_catch, MUID_catch, Old_Inlet, New_Inlet, Time_delay [s], individual values [s]")
        f.write("\n")
        for row in catch_time_data:
            f.write("%s,%s,%s,%s,%s,%s" % (row[0], row[1], row[2], row[3], row[4], row[5])) 
            f.write("\n")
    
    with open(path_txt+os.sep+'CatchTimeDataTrim'+'.txt', 'wb') as f:
        pickle.dump(catch_time_data, f)
    # return the time compensations
    return(catch_time_data, catch_tlist)
    
    
def CalculateVolumeLoss(pipe_adj, v_p_data, GenerateTextFiles, names, node_s, node_e, all_node_s, all_node_e, length, diameters, slope, FID_i, catch_muid, node_inlet, all_node_inlet, left_names_n, names_n, n_diameters, n_ground, n_inlev, path_txt, model):   
    # Provides compensations for volume losses in the simplified network.
    import math
    import pdb
    import sys, os
    print('Calculating volume losses')
    # deleted pipe flow-paths to each catchment
    pipe_vol=list()
    
    # Calculate volume of all links
    for i in range(len(names)):
        p_length=length[i]
        p_dia=diameters[i]
        # Calculate pipe volume
        vol=math.pi*math.pow(p_dia/float(2),2)*p_length
        pipe_vol.append(vol)
    
    catch_voldata=list()
    tmp_voldata=list()
    [tmp_voldata.append([]) for i in range(len(FID_i))]
    
    if model=='trim':
        pipes_del=left_names_n
        left_names_n=list()
        #List of remaining nodes
        node_s_red=[node_s[i] for i in range(len(names)) if not names[i] in pipes_del]
        node_e_red=[node_e[i] for i in range(len(names)) if not names[i] in pipes_del]
        
        #if a node name can no longer be found in node_s and node_e- mark for deletion
        left_names_n=node_s_red + node_e_red
      
    for i in range(len(FID_i)):
        if pipe_adj[i]:
            if model=='trim':
                del_node=list()
                n_vol=list()
                for element in pipe_adj[i]:
                    index=names.index(element)
                    p_length=length[index]
                    p_dia=diameters[index]
                    #Insert calculation of volume from deleted nodes here
                    if all_node_s[names.index(element)] in left_names_n:
                        del_node.append(all_node_s[names.index(element)])
                    elif all_node_e[names.index(element)] in left_names_n:
                        del_node.append(all_node_e[names.index(element)])
                    if del_node!=[]:
                        for node in del_node:
                            n_vol.append(math.pi *math.pow(n_diameters[names_n.index(node)]*0.5, 2)*(n_ground[names_n.index(node)]-n_inlev[names_n.index(node)]))
                    # Calculate pipe volume
                    try:
                        vol=math.pi*math.pow(p_dia/float(2),2)*p_length
                        # + volume in deleted manholes?
                        tmp_voldata[i].append(vol)
                    except:
                        tmp_voldata[i].append(0)
                        print(FID_i[i], pipe_adj[i])

            elif model=='merge':
                del_node=list()
                n_vol=list()
                for element in pipe_adj[i]:
                    # calculate deleted node volume
                    #select node(s) that has been deleted
                    if all_node_s[names.index(element)] not in left_names_n:
                        del_node.append(all_node_s[names.index(element)])
                    elif all_node_e[names.index(element)] not in left_names_n:
                        del_node.append(all_node_e[names.index(element)])
                    #node volume:
                    for node in del_node:
                        n_vol.append(math.pi *math.pow(n_diameters[names_n.index(node)]*0.5, 2)*(n_ground[names_n.index(node)]-n_inlev[names_n.index(node)]))
                    # calculate pipe difference volume
                    if v_p_data[names.index(element)]!=[]:
                        [tmp_voldata[i].append(j) for j in v_p_data[names.index(element)] if j !=[]]
            # summarize volumes for catchments
            tot_vol=math.fsum(tmp_voldata[i])+math.fsum(n_vol)
            catch_voldata.append([catch_muid[i],pipe_adj[i],tmp_voldata[i],tot_vol])
        
    with open(path_txt+os.sep+'CatchVolDataTrimfunc.txt', 'w') as f:
        f.write("Catchment MUID, Pipes deleted, All volume data [m3], Sum of volume data [m3]")
        f.write("\n")
        for row in catch_voldata:
            f.write("%s,%s,%s,%s" % (row[0], row[1], row[2], row[3])) 
            f.write("\n")

    tmp_vol=list()
    tmp_pipes=list()
    inlet_node_unique=list()
    tmp_vol_node=list()
    tmp_node=list()
    vol_node=list()
    checknode=list()

    [inlet_node_unique.append(node_inlet[catch_muid.index(row[0])]) for row in catch_voldata if node_inlet[catch_muid.index(row[0])] not in inlet_node_unique]

    [tmp_pipes.append([]) for i in range(len(inlet_node_unique))]
    [tmp_vol_node.append([]) for i in range(len(inlet_node_unique))]
    [tmp_node.append([]) for i in range(len(inlet_node_unique))]
    [vol_node.append([]) for i in range(len(inlet_node_unique))]
    
    if model=='merge':
        # save data to lists:
        catchments=[i[0] for i in catch_voldata]
        pipes=[i[1] for i in catch_voldata]
        volumes=[i[3] for i in catch_voldata]
        
        # for each catchment add the volume from pipes and nodes
        for catch in catch_muid:
            if catch in catchments:
                for element in pipes[catchments.index(catch)]:
                    ind_u=inlet_node_unique.index(node_inlet[catch_muid.index(catch)])
                    if element not in tmp_pipes[ind_u]:
                        del_node=list()
                        tmp_pipes[ind_u].append(element)
                        if all_node_s[names.index(element)] not in left_names_n:
                            if all_node_s[names.index(element)] not in checknode:
                                del_node.append(all_node_s[names.index(element)])
                        elif all_node_e[names.index(element)] not in left_names_n:
                            if all_node_e[names.index(element)] not in checknode:
                                del_node.append(all_node_e[names.index(element)])
                        else:
                            del_node=[]
                        
                        #node volume:
                        for node in del_node:
                            if node not in tmp_node[ind_u]:
                                tmp_vol_node[ind_u].append(math.pi *math.pow(n_diameters[names_n.index(node)]*0.5, 2)*(n_ground[names_n.index(node)]-n_inlev[names_n.index(node)]))
                                tmp_node[ind_u].append(node)                                           
                        # calculate pipe difference volume
                        if v_p_data[names.index(element)]!=[]:
                            try:
                                [tmp_vol_node[ind_u].append(j) for j in v_p_data[names.index(element)] if j !=[]]
                            except:
                                import pdb
                                pdb.set_trace()
                        [checknode.append(node) for node in del_node]
        
    # make list of volumes for each unique pipe connected to the inlet node
    if model=='trim':
        for catch in catch_muid:
            index=catch_muid.index(catch)
            
            for row in catch_voldata:
                
                if row[0]==catch:
                    for element in row[1]:
                        inlet_n=node_inlet[index]
                        ind_u=inlet_node_unique.index(inlet_n)

                        if element not in tmp_pipes[ind_u]:
                            if model=='trim':
                                del_node=list()
                                tmp_pipes[ind_u].append(element)
                                p_index=names.index(element)
                                tmp_vol_node[ind_u].append(pipe_vol[p_index])
                                
                                if all_node_s[names.index(element)] in left_names_n:
                                    if all_node_s[names.index(element)] not in checknode:
                                        del_node.append(all_node_s[names.index(element)])
                                elif all_node_e[names.index(element)] in left_names_n:
                                    if all_node_e[names.index(element)] not in checknode:
                                        del_node.append(all_node_e[names.index(element)])
                                else:
                                    del_node=[]
                                
                                if del_node!=[]:
                                    for node in del_node:
                                        tmp_vol_node[ind_u].append(math.pi*math.pow(n_diameters[names_n.index(node)]*0.5, 2)*(n_ground[names_n.index(node)]-n_inlev[names_n.index(node)]))
                                                            
                                [checknode.append(node) for node in del_node]
                            
                        # elif model=='merge':
                            # print('step 6')
                            # if all_node_s[names.index(element)] not in left_names_n:
                                # if all_node_s[names.index(element)] not in checknode:
                                    # del_node.append(all_node_s[names.index(element)])
                            # elif all_node_e[names.index(element)] not in left_names_n:
                                # if all_node_e[names.index(element)] not in checknode:
                                    # del_node.append(all_node_e[names.index(element)])
                            # else:
                                # del_node=[]
                            
                            # #node volume:
                            # for node in del_node:
                                # tmp_vol_node[ind_u].append(math.pi *math.pow(n_diameters[names_n.index(node)]*0.5, 2)*(n_ground[names_n.index(node)]-n_inlev[names_n.index(node)]))
                                                                               
                            # # calculate pipe difference volume
                            # if v_p_data[names.index(element)]!=[]:
                                # [tmp_vol_node[i].append(j) for j in v_p_data[names.index(element)] if j !=[]]
                            
                            # [checknode.append(node) for node in del_node]
    #summarize volumes for each inlet node:    
    
    for i in range(len(tmp_vol_node)):
        vol_node[i]=math.fsum(tmp_vol_node[i])
        
    # make list of new diameters
    #first calculate volume of manhole well
    node_volume=list()
    tot_vol_n=list()
    new_dia_n=list()
    for node in names_n:
        index=names_n.index(node)
        node_volume.append((math.pi*math.pow(n_diameters[index]/float(2),2))*(n_ground[index]-n_inlev[index]))

    for node in inlet_node_unique:
        in_index=inlet_node_unique.index(node)
        n_index=names_n.index(node)
        height=n_ground[n_index]-n_inlev[n_index]
        tot_vol_n.append(node_volume[n_index]+vol_node[in_index])
        try:
            # In some cases the compensation is negative, thus a maximum degree of vompensation is limited (the manhole cannot be smaller than nothing)
            # and to maintain connection to surface (a manhole smaller than 0.2 m may to a higher degree work as a throttle pipe to the surface, limiting water exchange) a minimum manhole diameter is set to 0.3m. 
            
            dia_test=round(2*math.sqrt(tot_vol_n[-1]/(height*math.pi)),2)
            if dia_test<0.2:
                new_dia_n.append(0.2)
            else:
                new_dia_n.append(round(2*math.sqrt(tot_vol_n[-1]/(height*math.pi)),2)) # Fixed: divide with Pi not multiply by Pi

        except:
             # in this case the new volume is smaller than the original and needs to be compensated for by more than the volume in the manhole.
             print('Negative volume compensation larger than manhole volume. Manhole diameter set to minimum value of 0.2m')
             new_dia_n.append(0.2)
             # pdb.set_trace()
             # new_dia_n.append(round(n_diameters[n_index],2))    
    
    if GenerateTextFiles:    
        # list of volumes change for each inlet node         
        with open(path_txt+os.sep+'CatchNodeVolDataTrimfunc.txt', 'w') as f:
            f.write('Description: Data of volume change for each inlet node')    
            f.write("\n")    
            f.write("Node Inlet, New diameter [m], Sum volume [m3], all volumes [m3]")
            f.write("\n")
            for i in range(len(inlet_node_unique)):
                f.write("%s,%s,%s %s" % (inlet_node_unique[i], new_dia_n[i], vol_node[i], tmp_vol_node[i])) 
                f.write("\n")
                
        p_data=list()
        for i in range(len(names)):
            p_data.append([names[i], round(length[i],4), diameters[i], round(slope[i]/float(100),3), round(pipe_vol[i],3), v_p_data[i]])

        with open(path_txt+os.sep+'CatchPipeData.txt', 'w') as f:
            f.write("Pipe MUID, Length [m], Diameter [m], Slope [0/00], Volume [m3], Velocity [m/s]")
            f.write("\n")
            for i in range(len(p_data)):
                f.write("%s" % (p_data[i])) 
                f.write("\n")  
    return(inlet_node_unique, new_dia_n)
    
    
def MakeRainSelectionLists(catch_tlist,catch_time_data, catch_muid, path_newselct): 
    from CleaningUtils import check_dir
    import sys, os
    
    print('Assigning new rain-series')
    # Selection files for catchments with new rain series
    td0, td2, td4, td6, td8, td10=[[] for i in range(6)]
    rain_list=list()
    name_list=['Rain_0min', 'Rain_2min', 'Rain_4min', 'Rain_6min', 'Rain_8min', 'Rain_10min']
    [rain_list.append(False) for i in range(len(name_list))]
    td=list()
    [td.append([]) for i in range(len(name_list))]
    for catch in catch_muid:
        if catch in catch_tlist:
            index=catch_tlist.index(catch)
            if catch_time_data[index][4]>120 and catch_time_data[index][4]<240:
                #time delay: 2min
                td[1].append(catch)
                rain_list[1]=True
            elif catch_time_data[index][4]>240 and catch_time_data[index][4]<360:
                #time delay: 4min
                td[2].append(catch)
                rain_list[2]=True
            elif catch_time_data[index][4]>360 and catch_time_data[index][4]<480:
                #time delay: 6min
                td[3].append(catch)
                rain_list[3]=True
            elif catch_time_data[index][4]>480 and catch_time_data[index][4]<600:
                #time delay: 8min
                td[4].append(catch)
                rain_list[4]=True
            elif catch_time_data[index][4]>600:
                #time delay: 10min
                td[5].append(catch)
                rain_list[5]=True
            else:
                #no time difference
                td[0].append(catch)
                rain_list[0]=True
        else:
            #no time difference
            td[0].append(catch)
            rain_list[0]=True
       
    #write data to text-files for inspection        

    selct_files=list()
    [selct_files.append(path_newselct+os.sep+'CatchSelect'+name_list[i][4:len(name_list[i])]+'.mus') for i in range(len(name_list))]
    
    # Save MIKE Urban selection file for each rain-series.
    for flist in selct_files:
        check_dir(flist)
        td_index=selct_files.index(flist)
        if rain_list[td_index]==True:
            with open(flist, 'w') as f:
                f.write("ms_Catchment")
                f.write("\n")
                for i in td[td_index]:
                    f.write("%s" % int(i)) 
                    f.write("\n") 
    return(name_list, rain_list)