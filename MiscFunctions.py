"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# Functions to restore network information in loops and function for calculation of flow velocity.

def RestoreNetworkConnections(loop,origin_check, s_node_pipes, e_node_pipes, s_node_pipes_full, e_node_pipes_full):
    # restore network connection information if features in a loop is accidently deleted. 
    if loop==True:
        for n in origin_check:
            if n in s_node_pipes:
                s_tmp=list(s_node_pipes_full[n])
                s_node_pipes[n]=s_tmp
            if n in e_node_pipes:
                e_tmp=list(e_node_pipes_full[n])
                e_node_pipes[n]=e_tmp
    return(s_node_pipes, e_node_pipes)

def CalculateVelocity(pipe, branch, slope, slope_tresh, t_p_adj, names, node_s, node_e, length, diameters, manning, level_lo, level_up):
    # Calculates flow velocity based on slope of the link.
    # If the calculated velocity is below 0.15m/s data for the next upstream link is used. 
    # If no link in the branch has sufficient flow velocity, the threshold of 0.15m/s is selected.
    import math 
    import pdb
    pindex=names.index(pipe)

    for pipe_new in branch[branch.index(pipe)+1:len(branch)]:
        pindex_next=names.index(pipe_new)
        if node_e[pindex]==node_e[pindex_next]:
            if pipe_new==branch[-1]:
                V=0.15 #manual estimation
                p_length=length[pindex]
                t_p_adj[pindex]=float(p_length)/V
            continue #if pipes are connected to same end node
        
        elif slope[pindex_next]>=0 and slope[pindex_next]<=slope_tresh:
            #if no slope in upstream node
            
            #if the pipe is the last in the branch (and still no slope)
            if pipe_new==branch[-1]:
                V=0.15 #manual estimation
                p_length=length[pindex]
                t_p_adj[pindex]=float(p_length)/V
            continue 
        
        elif slope[pindex_next]==0 and pipe_new==branch[-1]: #no pipes in branch with slope high enough
            V=0.15 # manual estimation of 15 cm/s (Butler and Davis, 2011)
            p_length=length[pindex]
            t_p_adj[pindex]=float(p_length)/V
            break
        
        else: # find new slope                            
            up_level=max([level_lo[pindex],level_up[pindex],level_lo[pindex_next],level_up[pindex_next]])
            lo_level=min([level_lo[pindex],level_up[pindex],level_lo[pindex_next],level_up[pindex_next]])
            length_tot=0
            for pipe_s in branch[branch.index(pipe):branch.index(pipe_new)+1]:
                length_tot=length_tot+length[names.index(pipe_s)]
            p_length=length[pindex]
            p_dia=diameters[pindex]
            p_manning=manning[pindex]
            p_slope=(up_level-lo_level)/length_tot
            hyd_rad=(math.pi*math.pow(p_dia/float(2),2))/(math.pi*p_dia) #Hydraulic radius
            
            #Manning equation:
            V=p_manning*math.pow(hyd_rad,float(2)/3)*math.pow(p_slope,float(1)/2)
            #if velocity < 15cm/s use another (higher) slope from another pipe for calculation
            if V < 0.15:
                #if the pipe is the last in the branch (and still no slope)
                if pipe_new==branch[-1]:
                    V=0.15 #manual estimation
                    p_length=length[pindex]
                    t_p_adj[pindex]=float(p_length)/V
                continue
            t_p_adj[pindex]=float(p_length)/V
            break
    return(V, t_p_adj)
        
