"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
def MakeNetworkFile(names_red, node_s_red, node_e_red, path_txt, att):
# Writes readable and importable lists of pipe and node connections as txt-files.
# MakeNetworkFile(PipeNames, StartNodes, EndNodes, OutputPath, SpecialAttribute(e.g. Trim400))
    
    import sys, os
    import arcpy
    import pickle
    
    nodecon_new=list()
    print('Generating link and start/end node connections')
    for i in range(len(names_red)):
        pipename=names_red[i]
        startnodenew=node_s_red[i]
        endnodenew=node_e_red[i]
        nodecon_new.append([[pipename],[startnodenew],[endnodenew]])
    
    #For import and use:
    with open(path_txt+os.sep+'linknode'+att+'.txt', 'wb') as f:
        pickle.dump(nodecon_new, f)
    
    #For visual inspection and debugging
    with open(path_txt+os.sep+'linknode'+att+'_inspection.txt', 'wb') as f:
        f.write('Pipe MUID, start-node, end-node')
        f.write('\n')
        for row in nodecon_new:
            f.write('%s, %s, %s' %(int(row[0][0]), int(row[1][0]), int(row[2][0])))
            f.write('\n')
            
    print('File saved as %s' % 'linknode'+att+'.txt')