"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

### Update boundary conditions of rain-input in the MIKE URBAN geodatabse.

def updateBoundary(name_list, rain_list):
    import arcpy
    import os
    import pdb
    pathbitem="in_memory/bitem"
    pathboundary="in_memory/boundary"
    path_select='..' + os.sep + 'SelectionFiles'
    
    
        
    # Extract fieldnames from database table
    print('Updating boundary conditions')       
    afield_bound=[f.name for f in arcpy.ListFields(pathboundary)]
    afield_bitem=[f.name for f in arcpy.ListFields(pathbitem)]
    
    # Make index of boundaries to update:
    rain_names=list()
    updatebound=[True for i in range(len(rain_list))]
    updatebitem=[True for i in range(len(rain_list))]
    for i in range(len(rain_list)):
        if rain_list[i]:
            rain_names.append(unicode('Rainfall'+name_list[i][4:len(name_list[i])]))
    i=0        
    # Change specific fields of the msm_BBoundary
    with arcpy.da.UpdateCursor(pathboundary,afield_bound) as cursor:
        for row in cursor:
            if i==0:
                origrow=row
                row[afield_bound.index('MUID')]=u'Rainfall_0min'
                row[afield_bound.index('ConnectionTypeNo')]=2
                row[afield_bound.index('ListName')]=path_select + os.sep +'CatchSelect'+name_list[0][4:len(name_list[0])]+'.mus'
                cursor.updateRow(row)
                i=1
            # Check if boundary condition is already in the boundary.
            if row[afield_bound.index('MUID')] in rain_names:
                updatebound[rain_names.index(row[afield_bound.index('MUID')])]=False
    
           
    # insert new rows in the table      
    boundrows=arcpy.InsertCursor(pathboundary)
    
    for i in range(1,len(name_list)):
        if rain_list[i]==True and updatebound[i]==True:
            row=boundrows.newRow()
            #copy original
            for element in range(len(origrow)):
                try:
                    field=afield_bound[element]
                    row.setValue(field,origrow[element])
                except:
                    print("can't overwrite %s" %field)
                
            #overwrite changed values
            row.setValue('MUID', unicode('Rainfall'+name_list[i][4:len(name_list[i])]))
            row.setValue('ConnectionTypeNo', 2) #list-type
            row.setValue('ListName', path_select + os.sep + 'CatchSelect'+name_list[i][4:len(name_list[i])]+'.mus') #selection list (.mus)

            boundrows.insertRow(row)

    # Change msm_BItem
    i=0    
    # update the original rain-series line to new rain-series name and list selection
    with arcpy.da.UpdateCursor(pathbitem,afield_bitem) as cursor:
        for row in cursor:
            if i==0:
                origrow_bitem=row
                row[afield_bitem.index('MUID')]=u'Rainfall_0min'
                row[afield_bitem.index('Description')]=u'Rainfall_0min'
                row[afield_bitem.index('BoundaryID')]=u'Rainfall_0min'
                row[afield_bitem.index('TSConnection')]=name_list[0]+'.dfs0'
                row[afield_bitem.index('DataTypeName')]=u'Rainfall Depth'
                cursor.updateRow(row)
                i=1
            #check if boundary condition is already inserted in boundary.
            if row[afield_bitem.index('MUID')] in rain_names:
                updatebitem[rain_names.index(row[afield_bitem.index('MUID')])]=False
    # add addition rain-series.
    bitemrows=arcpy.InsertCursor(pathbitem)
    for i in range(1,len(name_list)):
        if rain_list[i]==True and updatebitem[i]==True:
            row=bitemrows.newRow()
            #copy original:
            for element in range(len(origrow_bitem)):
                try:
                    field=afield_bitem[element]
                    row.setValue(field,origrow_bitem[element])
                except: 
                    print("can't overwrite %s" %field)
            #overwrite change values:
            row.setValue('MUID',unicode('Rainfall'+name_list[i][4:len(name_list[i])]))
            row.setValue('Description',unicode('Rainfall'+name_list[i][4:len(name_list[i])]))
            row.setValue('BoundaryID',unicode('Rainfall'+name_list[i][4:len(name_list[i])]))
            row.setValue('TSConnection',name_list[i]+'.dfs0')
            row.setValue('DataTypeName',u'Rainfall Depth')
            bitemrows.insertRow(row)
    
    # close the cursors
    del bitemrows 
    del boundrows