"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# Function for testing branches against the threshold for deletion. 
def checkbranches(diathreshmain, diathreshsecond, treshlengthsecond, node, elements, origin_check, visited, loop, s_node_pipes, e_node_pipes, names, diameters, length, s_node_pipes_full, e_node_pipes_full, node_s, node_e):
    #RECURSION: checks if the considered branch fulfills deleting criteria and extends the branch as long as the criteria are fulfilled (or an edge is reached)
    #node: current point for checking further upstream, elements - pipes found in the branch so far, origin_check - nodes that were already visited in the branch
    import math
    from MiscFunctions import RestoreNetworkConnections
    
    if node in origin_check:
        #this pipe section is a loop 
        loop=True 
        
    if not node in origin_check: 
        origin_check.append(node)
    visited.append(node)
    #check conditions for deleting (exceedence of pipe diameters)
    if not elements==[]:
        indices=[names.index(x) for x in elements]
        maxdiam=max([diameters[i] for i in indices])
        plength=[length[i] for i in indices]
        #check for conditions that prevent deleting
        if maxdiam>diathreshmain:
            [s_node_pipes, e_node_pipes] = RestoreNetworkConnections(loop,origin_check, s_node_pipes, e_node_pipes, s_node_pipes_full, e_node_pipes_full)
            return (False, [],[],visited,loop, s_node_pipes, e_node_pipes)
        if maxdiam>diathreshsecond and sum(plength)>=treshlengthsecond:
            [s_node_pipes, e_node_pipes] = RestoreNetworkConnections(loop,origin_check, s_node_pipes, e_node_pipes, s_node_pipes_full, e_node_pipes_full)
            return (False, [],[],visited,loop, s_node_pipes, e_node_pipes)
    #find connected pipes that do not lead to nodes that were considered already
    if node in s_node_pipes: 
        st_p=s_node_pipes[node]
        for p in st_p:
            if node_e[names.index(p)] in origin_check:
                del(st_p[st_p.index(p)])
    else:
        st_p=[]
    if node in e_node_pipes: 
        en_p=e_node_pipes[node]
        for p in en_p:
            if node_s[names.index(p)] in origin_check:
                del en_p[en_p.index(p)]
    else:
        en_p=[]
    #break if we do not find any pipes that lead to new nodes (but do not stop the search at other points -> return True)
    checklist= st_p + en_p
    if checklist == []: 
        # if a node have been visited and pipe connection removed in s- or e_node_pipes, this pipe should not be removed:

        return (True, elements, origin_check, visited,loop, s_node_pipes, e_node_pipes)
    #extend the branch by all pipes connected to the current node and try to go further upstream
    for p in st_p:
        if not p in elements: 
            elements.append(p)
        else:
            return (True, elements,origin_check, visited,loop, s_node_pipes, e_node_pipes) #return element list, but keep searching
        enode=node_e[names.index(p)]
        (test, elements,origin_check,visited,loop, s_node_pipes, e_node_pipes)=checkbranches(diathreshmain, diathreshsecond, treshlengthsecond,enode,elements,origin_check,visited,loop, s_node_pipes, e_node_pipes, names, diameters, length, s_node_pipes_full, e_node_pipes_full,node_s, node_e)
        if test==False:
            [s_node_pipes, e_node_pipes]=RestoreNetworkConnections(loop,origin_check, s_node_pipes, e_node_pipes, s_node_pipes_full, e_node_pipes_full)
            return (False, [],[],visited,loop, s_node_pipes, e_node_pipes) #stop the search since some of the found pipes did not fulfil the delete criteria
    for p in en_p:
        if not p in elements: 
            elements.append(p)
        else:
            return (True, elements,origin_check,visited,loop, s_node_pipes, e_node_pipes) #return element list, but keep searching
        snode=node_s[names.index(p)]
        (test, elements,origin_check,visited,loop, s_node_pipes, e_node_pipes)=checkbranches(diathreshmain, diathreshsecond, treshlengthsecond,snode,elements,origin_check,visited,loop, s_node_pipes, e_node_pipes, names, diameters, length, s_node_pipes_full, e_node_pipes_full,node_s, node_e)
        if test==False:
            [s_node_pipes, e_node_pipes]=RestoreNetworkConnections(loop,origin_check, s_node_pipes, e_node_pipes, s_node_pipes_full, e_node_pipes_full)
            return (False, [],[],visited,loop, s_node_pipes, e_node_pipes)
    #if none of the connected pipes violates the do not delete criteria - return the branch as deletable
    return (True, elements,origin_check, visited,loop, s_node_pipes, e_node_pipes)