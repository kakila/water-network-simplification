"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
### File and function for computing network connectivity lists. 
### The textfile with the outputs must be written in the format [link MUID, start node MUID, end node MUID]

def compute_distance(p1,p2):
    import math
    # compute distance between coordinates: compute_distance([x,y], [x,y])
    dx=p1[0]-p2[0]
    dy=p1[1]-p2[1]
    length=math.sqrt(pow(dx,2)+pow(dy,2))
    return length

def findnodes(nodes,pipes,pipe,pipelist,threshd):
    # Finds nodes that are within a threshold distance from start- and endpoints of 
    # pipes and assign the node MUID to a dictionary.
    # findnodes(Node dictionary with x,y coordinates, pipe dictionary with x,y coordinates, MUID of pipe, network connection list, threshold distance)
    pstartcoor=pipes[pipe][0]
    pendcoor=pipes[pipe][1]
    for node in nodes:
        nodecoor=nodes[node]
        if compute_distance(nodecoor,pstartcoor)<threshd:
            pipelist[pipe][1].append(node)
            if len(pipelist[pipe][1])>1:
                print('Error: Pipe has more than one startnode with this threshold')
                import pdb
                pdb.set_trace()
        
        elif compute_distance(nodecoor,pendcoor)<threshd:
            pipelist[pipe][2].append(node)
            if len(pipelist[pipe][2])>1:
                print('Error: Pipe has more than one endnode with this threshold')
                import pdb
                pdb.set_trace()
    # if no node was found within the threshold, increase the threshold
    if pipelist[pipe][1]==[] and pipelist[pipe][2]==[]:
        print('No start or end node found for pipe %s' %pipe)
        try:
            newtresh=newtresh+0.2
        except:
            newtresh=threshd+0.2
        print('Search radius increased to %s meters' %newtresh)
        pipelist=findnodes(nodes,pipes,pipe,pipelist,newtresh)
        return(pipelist)
    
    return(pipelist)
    
def NetConnectivity(pathdb,threshdist,txtfile):
    #Makes dictionary with coordinates for nodes and pipes start/end points. 

    from CleaningUtils import check_dir
    import arcpy
    import pickle
    import sys, os
    
    pipelist={}
    pipes={}
    
    # Load databse to RAM:
    print('loading database')
    pathlinks="in_memory/links"
    pathnodes="in_memory/nodes"
    arcpy.CopyFeatures_management(pathdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Link',pathlinks)
    arcpy.CopyFeatures_management(pathdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Node',pathnodes)

    # Retrieve data
    # Index:            0        1              
    fields =        ['SHAPE@', 'MUID']
    fields_node =   ['SHAPE@', 'MUID']
    
    # Dictionary of link start and end coordinates
    with arcpy.da.SearchCursor(pathlinks,fields) as cursorlinks:
        for row in cursorlinks:
            pipeID=row[1]
            startX=row[0].firstPoint.X
            startY=row[0].firstPoint.Y
            endX=row[0].lastPoint.X
            endY=row[0].lastPoint.Y
            pipes[pipeID]=[startX,startY],[endX,endY]
    
    nodes={}
    #Dictionary of node coordinates
    with arcpy.da.SearchCursor(pathnodes,fields_node) as cursornodes:
        for rown in cursornodes:
            nodeID=rown[1]
            X=rown[0].firstPoint.X
            Y=rown[0].firstPoint.Y
            nodes[nodeID]=X,Y
    
    # find network connectivity based on coordinates
    counter=0
    print('Generating network connectivity with a threshold of %s metres.' %(threshdist))
    for element in pipes:
        
        pipe=str(element)
        counter=counter+1
        if counter %500 ==0:
            print('Link %s of %s (%s%% finished)' %(counter,len(pipes), int(round((float(counter)/len(pipes))*100)) ))

        pipelist[pipe]=pipe,[],[]
        pipelist=findnodes(nodes,pipes,pipe,pipelist,threshdist)
    print('Link %s of %s (%s%% finished)' %(counter,len(pipes), int(round((float(counter)/len(pipes))*100)) ))    
    import pdb
    pdb.set_trace()
    # check directory for textfile
    check_dir(txtfile)
    
    #generate txt file for manual inspection:
    outfile=os.path.join(os.path.split(txtfile)[0],os.path.splitext(os.path.split(txtfile)[1])[0]+'inspection.txt')
    with open(outfile,'w') as f:
        f.write('Pipe-MUID, start-node, end-node')
        f.write("\n")
        for pipe in pipelist:
            f.write('%s, %s, %s' %(pipelist[pipe][0],pipelist[pipe][1][0],pipelist[pipe][2][0]))
            f.write("\n")
    f.close()
    
    #generate file with data to be loaded into simplifying scripts:
    exportlist=list() 
    for pipe in pipelist:
        exportlist.append([[pipelist[pipe][0]],pipelist[pipe][1],pipelist[pipe][2]]) # [pipe MUID, start-node, end-node]

    with open(txtfile, 'wb') as f:
        pickle.dump(exportlist, f)
    
    if arcpy.Exists(pathlinks):
        arcpy.Delete_management(pathlinks)
    if arcpy.Exists(pathnodes):
        arcpy.Delete_management(pathnodes)
    
    
    return(pipelist)    