"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
### Function going through the network node-by-node checking for links and branches to remove. 

def go_upstream(current,pipes_del,origin,visited,t_p_adj,t_adj,pipe_adj,v_p_data,recursion, s_node_pipes, e_node_pipes, names, diameters, length, slope, manning, node_s, node_e, level_up, level_lo, catch_muid, node_inlet, update_travelTime, FID_i, all_names_n, catch_line, s_node_pipes_full, e_node_pipes_full, nodecoor, diathreshmain, diathreshsecond, treshlengthsecond):
    #goes upstream from a starting point and calls "checkbranches" at each intersection for each connected pipe (except the one we came from), to see if there is an upstream branch that can be deleted
    #######################################################################################
    import arcpy
    from flowtime import flowtime
    from checkbranches import checkbranches
    
    #Trace recursion depth:
    recursion=recursion+1

    #include the starting node in the list of visited nodes
    if not current in origin: origin.append(current)
    visited.append(current)
    loop=False
    
    #find connected pipes and check if these connect to a node that was not yet visited
    if current in s_node_pipes: 
        s_p=s_node_pipes[current]
        [s_p.remove(p) for p in s_p if node_e[names.index(p)] in origin]
    else:
        s_p=[]
    if current in e_node_pipes: 
        e_p=e_node_pipes[current]
        [e_p.remove(p) for p in e_p if node_s[names.index(p)] in origin]
    else:
        e_p=[]
    #break if we do not find any new pipes connected
    checklist= s_p + e_p
    if checklist == []: 
        return (pipes_del,origin,visited,t_p_adj,t_adj,pipe_adj,v_p_data, s_node_pipes, e_node_pipes, node_inlet, catch_line)
       
    #for all connected pipes, try to find a deletable branch, if not deletable - go upstream, otherwise mark the associated pipes for deletion
    #for all pipes that have their starting point in "current"
    
    for p in s_p:
        #check if branch can be removed and get list of elements to delete
        (delete, branch, nodes_del,visited,loop, s_node_pipes, e_node_pipes) = checkbranches(diathreshmain, diathreshsecond, treshlengthsecond, node_e[names.index(p)],[p],[current],visited,loop, s_node_pipes, e_node_pipes, names, diameters, length, s_node_pipes_full, e_node_pipes_full,node_s, node_e)
        

        #if nothing to delete, go upstream
        if delete==False:
            (pipes_del,origin,visited,t_p_adj,t_adj,pipe_adj,v_p_data, s_node_pipes, e_node_pipes, node_inlet, catch_line) = go_upstream(node_e[names.index(p)],pipes_del,origin,visited,t_p_adj,t_adj,pipe_adj,v_p_data,recursion, s_node_pipes, e_node_pipes,  names, diameters, length, slope, manning, node_s, node_e, level_up, level_lo, catch_muid, node_inlet, update_travelTime, FID_i, all_names_n, catch_line, s_node_pipes_full, e_node_pipes_full, nodecoor, diathreshmain, diathreshsecond, treshlengthsecond)

        else:
            [pipes_del.append(element) for element in branch if not element in pipes_del]
            [origin.append(n) for n in nodes_del if not n in nodes_del]

            [t_p_adj,t_adj,pipe_adj,v_p_data]=flowtime(current,branch,t_p_adj,t_adj,pipe_adj,v_p_data, names, slope, length, diameters, manning, node_s, node_e, level_lo, level_up, catch_muid, node_inlet, update_travelTime)

            #Updating catchment assignments:
            for n in nodes_del:
                if n in node_inlet: 

                    reassign=[FID_i[x] for x in range(len(node_inlet)) if node_inlet[x]==n]

                    for fidi in reassign:
                        ind_i=FID_i.index(fidi)
                        node_inlet[ind_i]=current
                        
                        newpoint=nodecoor[all_names_n.index(current)]
                        newarray=arcpy.Array([catch_line[ind_i].firstPoint,newpoint])
                        catch_line[ind_i]=arcpy.Polyline(newarray)
                        
    #for all pipes that have their end point in "current" (otherwise the same as above)
    for p in e_p:  
        (delete, branch, nodes_del,visited,loop, s_node_pipes, e_node_pipes) = checkbranches(diathreshmain, diathreshsecond, treshlengthsecond, node_s[names.index(p)],[p],[current],visited,loop, s_node_pipes, e_node_pipes, names, diameters, length, s_node_pipes_full, e_node_pipes_full,node_s, node_e)
   
        #if nothing to delete, go upstream
        if delete==False:
           (pipes_del,origin,visited,t_p_adj,t_adj,pipe_adj,v_p_data, s_node_pipes, e_node_pipes, node_inlet, catch_line) = go_upstream(node_s[names.index(p)],pipes_del,origin,visited,t_p_adj,t_adj,pipe_adj,v_p_data,recursion, s_node_pipes, e_node_pipes, names, diameters, length, slope, manning, node_s, node_e, level_up, level_lo, catch_muid, node_inlet, update_travelTime, FID_i, all_names_n, catch_line, s_node_pipes_full, e_node_pipes_full, nodecoor, diathreshmain, diathreshsecond, treshlengthsecond)
 
        else:
            [pipes_del.append(element) for element in branch if not element in pipes_del]
            [origin.append(n) for n in nodes_del if not n in nodes_del]

            [t_p_adj,t_adj,pipe_adj,v_p_data]=flowtime(current,branch,t_p_adj,t_adj,pipe_adj,v_p_data, names, slope, length, diameters, manning, node_s, node_e, level_lo, level_up, catch_muid, node_inlet, update_travelTime)
                       
            #Updating catchment assignments:
            for n in nodes_del:
                if n in node_inlet: 

                    reassign=[FID_i[x] for x in range(len(node_inlet)) if node_inlet[x]==n]

                    for fidi in reassign:
                        
                        ind_i=FID_i.index(fidi)
                        node_inlet[ind_i]=current
                        
                        newpoint=nodecoor[all_names_n.index(current)]
                        newarray=arcpy.Array([catch_line[ind_i].firstPoint,newpoint])
                        catch_line[ind_i]=arcpy.Polyline(newarray)
                        
    return (pipes_del,origin,visited,t_p_adj,t_adj,pipe_adj,v_p_data, s_node_pipes, e_node_pipes, node_inlet, catch_line)
    
    