"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

### Save data to MIKE Urban geodatabase - For the trimming approach.

def SaveData(update_travelTime, update_catchments, update_boundaries, pathnewdb, pipes_del, names, node_s, node_e, level_up, level_lo, pipecoor_s, pipecoor_e, FID_n, names_n, inlet_node_unique, new_dia_n, FID_i, node_inlet, catch_line, all_FID_n, all_names_n):
    import arcpy
    import sys, os
    import pickle
    import pdb
    
    pathlinks="in_memory/links"
    pathnodes="in_memory/nodes"
    pathcatch="in_memory/catchcon"
    pathcatchcon="in_memory/catchconmsm"
    pathcatchconlink="in_memory/msmcatchconlink"
    pathbitem="in_memory/bitem"
    pathboundary="in_memory/boundary"
    
    # fields to be loaded from each databse:
    # Index:     0       1         2        3       4        5          6         7        8           9        10       11      12
    fields = ['SHAPE@','MUID','Diameter','Length','OID@','UpLevel', 'DwLevel', 'Height', 'Width', 'Length_C','TypeNo','Slope_C', 'Manning']
    fields_node = ['SHAPE@', 'MUID', 'OID@', 'Diameter','InvertLevel','Groundlevel']
    fields_catchcon = ['OBJECTID','MUID','node_id']
    fields_catchconmsm = ['OBJECTID','MUID','NodeID']
    fields_catchconlink= ['OBJECTID','MUID','SHAPE@']

    print('Updating link database')

    with arcpy.da.UpdateCursor(pathlinks,fields) as cursorl:
        for row in cursorl:
            if row[1] in pipes_del:#not in names_red:
                cursorl.deleteRow()

    #remove deleted pipes and associated nodes from the original name lists
    names_red=[names[i] for i in range(len(names)) if not names[i] in pipes_del]
    node_s_red=[node_s[i] for i in range(len(names)) if not names[i] in pipes_del]
    node_e_red=[node_e[i] for i in range(len(names)) if not names[i] in pipes_del]
        
    #if a node name can no longer be found in node_s and node_e- mark for deletion
    checklist=node_s_red + node_e_red

    print('Updating node database')

    rest_nodes=list()
    i=0
    FID_n_del=[FID_n[i] for i in range(len(names_n)) if not str(names_n[i]) in checklist]
    with arcpy.da.UpdateCursor(pathnodes,fields_node) as cursorn:
        for row in cursorn:
            if row[2] in FID_n_del:
                cursorn.deleteRow()
            elif update_travelTime==True:
                if row[1] in inlet_node_unique:
                    ind=inlet_node_unique.index(row[1])
                    row[3]=new_dia_n[ind]
                    cursorn.updateRow(row)
                   
    if update_catchments==True:
            
        # Update inlet nodes for catchments:
        print('Updating catchment database 1/3')
        with arcpy.da.UpdateCursor(pathcatch,fields_catchcon) as cursorc:
            for row in cursorc: 
                for fidc in FID_i:
                    if row[0]==fidc:
                        index=FID_i.index(fidc)
                        row[2]=node_inlet[index]
                        cursorc.updateRow(row)
                        
        # Update inlet nodes for catchments:
        print('Updating catchment database 2/3')
        with arcpy.da.UpdateCursor(pathcatchcon,fields_catchconmsm) as cursorcon:
            for row in cursorcon: 
                for fidc in FID_i:
                    if row[0]==fidc:
                        index=FID_i.index(fidc)
                        row[2]=node_inlet[index]
                        cursorcon.updateRow(row)
                        
        # Update catchment connection lines
        print('Updating catchment database 3/3')
        with arcpy.da.UpdateCursor(pathcatchconlink,fields_catchconlink) as cursorcconlink:
            for row in cursorcconlink:
                row[2]=catch_line[FID_i.index(int(row[0]))]
                cursorcconlink.updateRow(row)
            
    #Delete old databases:
    print('Deleting old databases')
    if arcpy.Exists(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Link'):
        arcpy.Delete_management(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Link')
    if arcpy.Exists(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Node'):
        arcpy.Delete_management(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Node')

    if arcpy.Exists(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_CatchConLink') and update_catchments==True:
        arcpy.Delete_management(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_CatchConLink')
    if arcpy.Exists(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'ms_Catchment') and update_catchments==True:
        arcpy.Delete_management(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'ms_Catchment')
    if arcpy.Exists(pathnewdb + os.sep + 'msm_CatchCon') and update_catchments==True:
        arcpy.Delete_management(pathnewdb + os.sep + 'msm_CatchCon')
    if arcpy.Exists(pathnewdb + os.sep + 'msm_BBoundary') and update_boundaries==True:
        arcpy.Delete_management(pathnewdb + os.sep + 'msm_BBoundary')
    if arcpy.Exists(pathnewdb + os.sep + 'msm_BItem') and update_boundaries==True:
        arcpy.Delete_management(pathnewdb + os.sep + 'msm_BItem')

    #Write new databases

    print('Writing new databases')

    arcpy.CopyFeatures_management(pathlinks, pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Link')
    arcpy.CopyFeatures_management(pathnodes, pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Node')
    if update_catchments==True:
        arcpy.CopyFeatures_management(pathcatchconlink, pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_CatchConLink')
        arcpy.CopyFeatures_management(pathcatch, pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'ms_Catchment')
        arcpy.CopyRows_management(pathcatchcon, pathnewdb + os.sep + 'msm_CatchCon')
    if update_boundaries==True:
        arcpy.CopyRows_management(pathboundary, pathnewdb + os.sep + 'msm_BBoundary')
        arcpy.CopyRows_management(pathbitem, pathnewdb + os.sep + 'msm_BItem')

    # Create Geometric Network
    dest = pathnewdb + '/mu_Geometry'
    arcpy.CreateGeometricNetwork_management(dest, "msm_Net", "msm_CurbInlet SIMPLE_EDGE NO;msm_Link SIMPLE_EDGE NO;msm_Node SIMPLE_JUNCTION NO;msm_Orifice SIMPLE_EDGE NO;msm_Pump SIMPLE_EDGE NO;msm_Valve SIMPLE_EDGE NO;msm_Weir SIMPLE_EDGE NO", "0.05")
    print('Database succesfully updated!')    
    
        #Make a list of remaining nodes for the couple file
    for fidn in all_FID_n:
        if fidn not in FID_n_del:
            ind=all_FID_n.index(fidn)
            rest_nodes.append(all_names_n[ind])
    for i in range(len(node_inlet)):
        if node_inlet[i] not in rest_nodes:
            if node_inlet[i]=='17593':
                node_inlet[i]='17590'

    return(rest_nodes, names_red, node_s_red, node_e_red)
    