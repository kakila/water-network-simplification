"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
#Some functions that can handle None values in type conversion
def str_robust(x): 
    if not x is None: return str(x)
    else: return None

def int_robust(x): 
    if not x is None: return int(x)
    else: return None

def float_robust(x): 
    if not x is None: return float(x)
    else: return None
   
#Unify a sequence
def uniquify(seq): # Dave Kirby
    # Order preserving
    seen = set()
    return [x for x in seq if x not in seen and not seen.add(x)]
    
def get_connected(nd,dict):
    #return pipe names saved as list in dictionary, if the node exists in the dictionary, otherwise return empty list
    if nd in dict:
        if not isinstance(dict[nd],basestring): #check if we have a string or a list saved in the dictionary
            return dict[nd]
        else:
            return [dict[nd]]
    else:
        return []

# Check for parallel links.
def check_parallel(p,s_node_pipes,e_node_pipes,n_s,n_e):
    #get pipes connected to start and end node
    p_s=get_connected(n_s,s_node_pipes)+get_connected(n_s,e_node_pipes)
    p_e=get_connected(n_e,s_node_pipes)+get_connected(n_e,e_node_pipes)
    #get all pipes that are connected both to the same start and the end node
    test=[x for x in p_s if x in p_e]
    #remove the considered pipe from the test list
    test=[x for x in test if not x==p]
    if len(test)>0:
        return True
    else:
        return False

# Check and make new directories.        
def check_dir(f):
    import sys, os
    # check if a directory to a file exist and creates it if not 
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)
        print('New directory created to %s' %d)
        
def check_fold(f):
    import sys, os
    #Check if a folder exist and creates it of not
    d = f
    if not os.path.exists(d):
        os.makedirs(d)
        print('New directory created to %s' %d)