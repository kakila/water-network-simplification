"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

### Save data to MIKE Urban geodatabase - For the merging approach.

def SaveData(update_travelTime, update_catchments, update_boundaries, pathnewdb, names_n, names, all_names, new_polylines, new_dia, new_length, new_uplevel, new_lolevel, new_height, new_width, FID_i, newconnodes, catch_line, nodesdel, inlet_node_unique, new_dia_n):
    import arcpy
    import sys, os
    import pickle
    
    
    
    pathlinks="in_memory/links"
    pathnodes="in_memory/nodes"
    pathcatch="in_memory/catchcon"
    pathcatchcon="in_memory/catchconmsm"
    pathcatchconlink="in_memory/msmcatchconlink"
    pathbitem="in_memory/bitem"
    pathboundary="in_memory/boundary"
    
    # fields to be loaded from each databse:
    # Index:     0       1         2        3       4        5          6         7        8           9        10       11      12
    fields = ['SHAPE@','MUID','Diameter','Length','OID@','UpLevel', 'DwLevel', 'Height', 'Width', 'Length_C','TypeNo','Slope_C', 'Manning']
    fields_node = ['SHAPE@', 'MUID', 'OID@', 'Diameter','InvertLevel','Groundlevel']
    fields_catchcon = ['OBJECTID','MUID','node_id']
    fields_catchconmsm = ['OBJECTID','MUID','NodeID']
    fields_catchconlink= ['OBJECTID','MUID','SHAPE@']
    pipes_del=list()
    [pipes_del.append(pipe) for pipe in all_names if pipe not in names]

            
    
    print('Updating link database')
    with arcpy.da.UpdateCursor(pathlinks,fields) as cursorl:
        for row in cursorl:
            if row[1] in pipes_del:
                cursorl.deleteRow()
            else:
                ind=all_names.index(row[1])
                row[0]=new_polylines[ind]   #Polyline
                row[2]=new_dia[ind]         #Diameter
                row[3]=new_length[ind]      #length
                row[5]=new_uplevel[ind]     #UpLevel
                row[6]=new_lolevel[ind]     #DwLevel
                row[7]=new_height[ind]      #Height
                row[8]=new_width[ind]       #Width
                row[9]=new_length[ind]      #length_C
                # Slope?
                cursorl.updateRow(row)


    #update inlet nodes (1 of 2)
    print('Updating catchment assignments 1/3')
    with arcpy.da.UpdateCursor(pathcatch,fields_catchcon) as cursorc:
        for row in cursorc:
            row[2]=newconnodes[FID_i.index(int(row[0]))]
            cursorc.updateRow(row)
            #update inlet layer
    
    #update inlet nodes (2 of 2)
    print('Updating catchment assignments 2/3')
    with arcpy.da.UpdateCursor(pathcatchcon,fields_catchconmsm) as cursorccon:
        for row in cursorccon:
            row[2]=newconnodes[FID_i.index(int(row[0]))]
            cursorccon.updateRow(row)

    #update catchment connection polylines
    print('Updating catchment assignments 3/3')
    with arcpy.da.UpdateCursor(pathcatchconlink,fields_catchconlink) as cursorcconlink:
        for row in cursorcconlink:
            row[2]=catch_line[FID_i.index(int(row[0]))]
            cursorcconlink.updateRow(row)

    # update node layer - delete nodes and insert volume compensations 
    print('Updating node database')      
    with arcpy.da.UpdateCursor(pathnodes,fields_node) as cursorn:
        for row in cursorn:
            if row[1] in nodesdel:
                cursorn.deleteRow()
            elif update_travelTime==True:
                if row[1] in inlet_node_unique:
                    ind=inlet_node_unique.index(row[1])
                    row[3]=new_dia_n[ind]
                    cursorn.updateRow(row)
            
    #Delete old databases:
    print('Deleting old databases')
    if arcpy.Exists(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Link'):
        arcpy.Delete_management(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Link')
    if arcpy.Exists(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Node'):
        arcpy.Delete_management(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Node')

    if arcpy.Exists(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_CatchConLink') and update_catchments==True:
        arcpy.Delete_management(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_CatchConLink')
    if arcpy.Exists(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'ms_Catchment') and update_catchments==True:
        arcpy.Delete_management(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'ms_Catchment')
    if arcpy.Exists(pathnewdb + os.sep + 'msm_CatchCon') and update_catchments==True:
        arcpy.Delete_management(pathnewdb + os.sep + 'msm_CatchCon')
    if arcpy.Exists(pathnewdb + os.sep + 'msm_BBoundary') and update_boundaries==True:
        arcpy.Delete_management(pathnewdb + os.sep + 'msm_BBoundary')
    if arcpy.Exists(pathnewdb + os.sep + 'msm_BItem') and update_boundaries==True:
        arcpy.Delete_management(pathnewdb + os.sep + 'msm_BItem')

    #Write new databases

    print('Writing new databases')

    arcpy.CopyFeatures_management(pathlinks, pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Link')
    arcpy.CopyFeatures_management(pathnodes, pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Node')
    if update_catchments==True:
        arcpy.CopyFeatures_management(pathcatchconlink, pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_CatchConLink')
        arcpy.CopyFeatures_management(pathcatch, pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'ms_Catchment')
        arcpy.CopyRows_management(pathcatchcon, pathnewdb + os.sep + 'msm_CatchCon')
    if update_boundaries==True:
        arcpy.CopyRows_management(pathboundary, pathnewdb + os.sep + 'msm_BBoundary')
        arcpy.CopyRows_management(pathbitem, pathnewdb + os.sep + 'msm_BItem')

    # Create Geometric Network
    dest = pathnewdb + '/mu_Geometry'
    arcpy.CreateGeometricNetwork_management(dest, "msm_Net", "msm_CurbInlet SIMPLE_EDGE NO;msm_Link SIMPLE_EDGE NO;msm_Node SIMPLE_JUNCTION NO;msm_Orifice SIMPLE_EDGE NO;msm_Pump SIMPLE_EDGE NO;msm_Valve SIMPLE_EDGE NO;msm_Weir SIMPLE_EDGE NO", "0.05")
    print('Database succesfully updated!')    
    
    
    #Make a list of remaining nodes for the couple file
    rest_nodes=list()
    for node in names_n:
        if node not in nodesdel:
            rest_nodes.append(node)
    
    # for fidn in all_FID_n:
        # if fidn not in FID_n_del:
            # ind=all_FID_n.index(fidn)
            # rest_nodes.append(all_names_n[ind])
    # for node in node_inlet:
        # if node not in rest_nodes:
            # pdb.set_trace()
    return(rest_nodes)
    