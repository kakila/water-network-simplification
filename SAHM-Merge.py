"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
### Main script for the merging approach in SAHM: Merges short pipes with other connected pipes from MIKE URBAN geodatabase (.gdb)

print('Initializing...')
import sys, os
import pdb
import sys
#################################################################
#append script directory to PATH variable
wd=os.path.dirname(os.path.realpath(__file__))
sys.path.append(wd)

sys.path.append(r'C:\Program Files (x86)\ArcGIS\Desktop10.1\arcpy')
sys.path.append(r'c:\Program Files (x86)\ArcGIS\Desktop10.1\bin')

import shutil
from CleaningUtils import *
from UpdateCoupleFile import UpdateCoupleFile
from MakeNetworkFile import *
import arcpy
import numpy as np
import math
import time
import traceback
from time import strftime
import pickle
from mergeUtils import *
from mergeCheck import mergeCheck
from mergeCheck import merge
from LoadData import *
from NetworkConnectivity import NetConnectivity



##############################################
################ USER INPUTS #################
##############################################

######### TRIM-MERGE-MODEL #########
#Specify if this is a merge on a previously trimmed model:
TrimMerge=False
#Specify name of trimmed model to base the merge on.
trimlevel='450'

######### MODULES #########
# Generate network connectivity-file (or load a previously generated one):
GenerateNetworkConnectivity=True

# Generate network connectivity file for the simplified model. 
SaveNetworkConnectivity=True   # Generate network connectivity file for later use. 

# To exclude compensation set the following to False:
includeCompensations=True

######### THRESHOLDS #########
# Set threshold for merging: Links shorter than 'minlength' will be merged. 
# Unit is metres.
minlength=45

# Set threshold for maximum difference in diameter between two pipes to be merged. 
# Specified as decimal between 0-1. Recommended value is 0.1 (10% difference). 
thresh_diam=0.1 

######### Folder attributes #########
# Make compensations for travel time and volume losses.
if includeCompensations:
    update_travelTime=True
    update_boundaries=True
else:
    update_travelTime=False
    update_boundaries=False
    
if TrimMerge==True:
    att='Trim'+trimlevel+'Merge'+str(minlength)+'m'
    if update_travelTime==False:
        att=att+'-noComp'
else:
    att='Merge'+str(minlength)+'m'
    if update_travelTime==False:
        att=att+'-noComp'

######### SOURCE FILES AND DATABSE #########        
indir=os.path.abspath(r'c:\Users\rolo\Documents\rolo\Teaching\StudentProjects\2017_Simplification_1D\Data\Model')
outdir=os.path.abspath(r'c:\Users\rolo\Documents\rolo\Teaching\StudentProjects\2017_Simplification_1D\Data\TEST')
dbname='12124_02.gdb'
couplename='1D2D_NetworkDANCE.couple'

# Specify path to original and new database, .couple file and network connectivity file(optional).
# Parent model
pathnetconn=os.path.join(indir,'ConnectivityFiles','linknode_full.txt')         # Network connectivity of parent model (if already generated, otherwise generate by setting GenerateNetworkConnectivity = True)
pathdb=os.path.join(indir,dbname)                           # Original MIKE URBAN Geodatabse (Parent model)
pathcouple=os.path.join(indir,couplename)                   # Original MIKE Flood couple-file
        
# New model     
pathnewdb=os.path.join(outdir,att,dbname)                   # New database with simplified model
pathnewcouple=os.path.join(outdir,att,couplename)           # New MIKE Flood couple-file

#specify path to save generated selection files:
path_newselct=os.path.join(outdir,att,'SelectionFiles')     # Catchment selection files for rain-series
path_txt=os.path.join(outdir,att,'Textfiles')               # Directory for txt-files containing raw data for e.g. volume compensations and travel-time.

########## GENERATE NETWORK CONNECTIVITY ############
# only needed once for the full network as output is saved to txt-files
if GenerateNetworkConnectivity:
    # set threshold distance between coordinates in metres
    threshdist= 0.1 # [m]

    #run function
    pipelist=NetConnectivity(pathdb, threshdist, pathnetconn)

    
print('Saving merged model to "%s"' %pathnewdb)
##############################################
############ END OF USER INPUTS ##############
##############################################    

###################################
############ LOAD DATA ############
###################################
[geom, rows, FID_p, names, diameters, heights, widths, length, length_c, slope, manning, level_lo, level_up, crs, pipecoor_s, pipecoor_e, vol_diff, v_p_data, CatchAddLength, pipe_adj, all_names, all_diameters, all_length, all_slope, 
new_uplevel, new_lolevel, new_polylines, new_dia, new_height, new_width, new_length, del_rows, 
FID_n, all_FID_n, row_nodes, names_n, all_names_n, nodecoor, n_diameters, n_inlev, n_ground, outlets,
FID_i, catch_muid, node_inlet, all_node_inlet, t_adj, newconnodes, oldconnodes, FID_i_link, catch_line, node_s, node_e, all_node_s, all_node_e, s_node_pipes, e_node_pipes, s_node_pipes_full, e_node_pipes_full, nodesdel
]=LoadData(pathdb, pathcouple, pathnetconn, pathnewdb, pathnewcouple, path_newselct, path_txt, TrimMerge, GenerateNetworkConnectivity)    
    
###################################
########### MAIN SCRIPT ###########
###################################
        
##### SELECT AND MERGE LINKS ######
[CatchAddLength, t_adj, pipe_adj, v_p_data, node_inlet, pipe_del, node_s, node_e, names_n
]=merge(minlength, thresh_diam, geom, rows, 
FID_p, names, diameters, heights, widths, length, length_c, slope, manning, level_lo, level_up, crs, 
pipecoor_s, pipecoor_e, vol_diff, v_p_data, CatchAddLength, pipe_adj, all_names, all_diameters, 
all_length, all_slope, new_uplevel, new_lolevel, new_polylines, new_dia, new_height, new_width, 
new_length, del_rows, FID_n, all_FID_n, row_nodes, names_n, all_names_n, nodecoor, n_diameters, 
n_inlev, n_ground, FID_i, catch_muid, node_inlet, t_adj, newconnodes, oldconnodes, FID_i_link, 
catch_line, node_s, node_e, s_node_pipes, e_node_pipes, s_node_pipes_full, e_node_pipes_full, nodesdel)



###################################################
###### CALCULATE AND IMPLEMENT COMPENSATIONS ######
###################################################

# Travel time 1/3:
# Calculates flows in links and branches
from flowtime import flowtimeMerge
(t_adj, pipe_adj)=flowtimeMerge(CatchAddLength, t_adj, catch_muid, pipe_adj)

# Travel time 2/3:
# Calculate time delay for catchments.
from compensations import TravelTime
trimdata=r'C:\Models\Databases\Trim\Trim' + trimlevel + os.sep+ 'Textfiles' +os.sep + 'CatchTimeDataTrim.txt'
(catch_time_data, catch_tlist)=TravelTime(t_adj, node_inlet, FID_i, catch_muid, oldconnodes, path_txt, TrimMerge, trimdata)

# Travel time 3/3:
# Assign catchments to use the delayed rain-series
from compensations import MakeRainSelectionLists
(name_list, rain_list)=MakeRainSelectionLists(catch_tlist, catch_time_data, catch_muid, path_newselct)

# Volume losses:
# Calculate volume losses and new manhole diameters.
from compensations import CalculateVolumeLoss
GenerateTxtFiles=False
(inlet_node_unique, new_dia_n)=CalculateVolumeLoss(pipe_adj, v_p_data, GenerateTxtFiles, all_names, node_s, node_e, all_node_s, all_node_e, all_length, all_diameters, all_slope, FID_i, catch_muid, node_inlet, all_node_inlet, names_n, all_names_n, n_diameters, n_ground, n_inlev, path_txt, 'merge')



########################################
########### UPDATE DATABASES ###########
########################################

# Update boundary conditions
from updateBoundaries import *
updateBoundary(name_list, rain_list)        

# Update links, nodes and catchment database layers (save updates)
from SaveDataMerge import *
(rest_nodes)=SaveData(update_travelTime, True, update_boundaries, pathnewdb, names_n, names, all_names, new_polylines, new_dia, new_length, new_uplevel, new_lolevel, new_height, new_width, FID_i, newconnodes, catch_line, nodesdel, inlet_node_unique, new_dia_n)

# Update the MIKE Flood couple to remaining nodes
UpdateCoupleFile(rest_nodes,pathcouple,pathnewcouple,False)
print('Couple-file updated')

# Save network connectivity information
if SaveNetworkConnectivity:
    # MakeNetworkFile(PipeNames, StartNodes, EndNodes, OutputPath, SpecialAttribute(e.g. Trim400))
    MakeNetworkFile(names, node_s, node_e, path_txt,att)

print('Merge script finished succesfully at %s' % strftime("%Y-%m-%d %H:%M:%S"))