"""
@file
@author     Roland Loewe <rolo@env.dtu.dk> & Steffen Davidsen <stda@env.dtu.dk>
@Institute  Department of Environmental Engineering, Technical University of Denmark (DTU)
@version    1.0
@date       December 2016
@section    LICENSE
SAHM - Simplification Algorithm for 1D Hydraulic network Models
Copyright (C) 2016  Steffen Davidsen & Roland Loewe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
### Loads MIKE URBAN geodatabase layers to RAM and save data in lists and dictionaries.

def LoadData(ingdb, incouple, innetwork, newdb, newcouple, newselct, pathtxt, TrimMerge, GenerateNetworkConnectivity):
    import sys, os
    import pdb
    import sys
    #################################################################
    #append script directory to PATH variable
    wd=os.path.dirname(os.path.realpath(__file__))
    sys.path.append(wd)
    #import arcpy
    sys.path.append(r'C:\Program Files (x86)\ArcGIS\Desktop10.1\arcpy')
    sys.path.append(r'c:\Program Files (x86)\ArcGIS\Desktop10.1\bin')
    import arcpy
    import sys, os
    import pickle
    from CleaningUtils import *
    
    print('Loading databases')
    # Assigning paths to data
    pathnetconn=innetwork
    pathdb=ingdb
    pathcouple=incouple
    pathnewdb=newdb
    pathnewcouple=newcouple
    path_txt=pathtxt
    path_newselct=newselct
    #Check if output directory exist and create it if it doesn't
    check_dir(pathnewdb)
    check_dir(pathnewcouple)
    check_fold(path_txt)
    check_fold(path_newselct)
    
    
    #Copy database to new location or overwrite already existing
    if not arcpy.Exists(pathnewdb):
        arcpy.Copy_management(pathdb,pathnewdb)
    else:
        print('WARNING! Database already exist. Data will be overwritten')
    
    # Delete msm_Net file to be able to edit data. Recreate after modification has finished.
    if arcpy.Exists(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Net'):
        arcpy.Delete_management(pathnewdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Net')
    
    #Copy tables to RAM
    print('Copying database layers to RAM')
    
    pathlinks="in_memory/links"
    pathnodes="in_memory/nodes"
    pathcatch="in_memory/catchcon"
    pathcatchcon="in_memory/catchconmsm"
    pathcatchconlink="in_memory/msmcatchconlink"
    pathbitem="in_memory/bitem"
    pathboundary="in_memory/boundary"
    
    arcpy.CopyFeatures_management(pathdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Link',pathlinks)
    arcpy.CopyFeatures_management(pathdb + os.sep + 'mu_Geometry' + os.sep + 'msm_Node',pathnodes)
    arcpy.CopyFeatures_management(pathdb + os.sep + 'mu_Geometry' + os.sep + 'ms_Catchment',pathcatch)
    arcpy.CopyFeatures_management(pathdb + os.sep + 'mu_Geometry' + os.sep + 'msm_CatchConLink',pathcatchconlink)
    arcpy.CopyRows_management(pathdb + os.sep + 'msm_CatchCon',pathcatchcon)
    arcpy.CopyRows_management(pathdb + os.sep + 'msm_BItem',pathbitem)
    arcpy.CopyRows_management(pathdb + os.sep + 'msm_BBoundary',pathboundary)
    
    
    # fields to be loaded from each databse:
    # Index:     0       1         2        3       4        5          6         7        8           9        10       11      12
    fields = ['SHAPE@','MUID','Diameter','Length','OID@','UpLevel', 'DwLevel', 'Height', 'Width', 'Length_C','TypeNo','Slope_C', 'Manning']
    fields_node = ['SHAPE@', 'MUID', 'OID@', 'Diameter','InvertLevel','Groundlevel', 'TypeNo']
    fields_catchcon = ['OBJECTID','MUID','node_id']
    fields_catchconmsm = ['OBJECTID','MUID','NodeID']
    fields_catchconlink= ['OBJECTID','MUID','SHAPE@']
    
    #Define empty lists for data
    
    ### Link data ###
    geom, rows, FID_p, names, diameters, heights, widths, length, length_c, slope, manning, level_lo, level_up, crs, pipecoor_s, pipecoor_e, p_emtylist, p_emtylist2, pipe_adj =[[] for i in range(19)]
    all_names, new_uplevel, new_lolevel, new_polylines, new_dia, new_height, new_width, new_length, del_rows, all_diameters, all_length, all_slope=[[] for i in range(12)]
    
    ### Node data ###
    FID_n, all_FID_n, row_nodes, names_n, typeNo, all_names_n, nodecoor, n_diameters, n_inlev, n_ground, outlets=[[] for i in range(11)]
    
    ### Network data ###
    nodecon, linkage=[[] for i in range(2)]
    
    ### Catchment data ###
    FID_i, catch_muid, node_inlet, all_node_inlet, t_adj, newconnodes, oldconnodes, FID_i_link, catch_line=[[] for i in range(9)]
    
    ### Other lists and dictionaries ###
    emptylist_1=list()
    c_emptylist=list()

    s_node_pipes={} #dictionaries of pipes connected each node with their start / end points
    e_node_pipes={}
    s_node_pipes_full={} #version that will not be modified. For restoration of deleted data in loops
    e_node_pipes_full={}
    
    #### LINK DATA ####
    print('Loading link data')
    with arcpy.da.SearchCursor(pathlinks,fields) as cursorli:
        for row in cursorli:
            geom.append(row[0]) #geometry
            rows.append(row)    #all data
            FID_p.append(row[4])
            names.append(str_robust(row[1]))
            diameters.append(float_robust(row[2]))
            heights.append(float_robust(row[7]))
            widths.append(float_robust(row[8]))
            plen=float_robust(row[3])
            if plen is None or plen==0.0:
                plen=float_robust(row[9])
                if plen is None or plen==0.0:
                    plen=row[0].getLength()
            plen=round(float_robust(plen),1)
            length.append(plen) #we don't use this, but the geometries feature length computed below!
            length_c.append(plen)
            slopecalc=abs((row[5]-row[6])/plen*100)
            slope.append(float_robust(slopecalc))
            manning.append(float_robust(row[12]))
            level_lo.append(float_robust(row[6]))
            level_up.append(float_robust(row[5]))
            crs.append(row[10]) #type 1: circular, 2: v-shape   3: rectangular
            pipecoor_s.append(row[0].firstPoint)
            pipecoor_e.append(row[0].lastPoint)
            p_emtylist.append([]) #t_p_adj
            p_emtylist2.append([])
            
            all_names.append(str_robust(row[1]))
            all_diameters.append(float_robust(row[2]))
            all_slope.append(float_robust(slopecalc))
            all_length.append(plen)
            new_uplevel.append(float_robust(row[5]))
            new_lolevel.append(float_robust(row[6]))
            new_polylines.append(row[0])
            new_dia.append(float_robust(row[2]))
            new_height.append(float_robust(row[7]))
            new_width.append(float_robust(row[8]))
            new_length.append(plen)
 

    #### NODE DATA ####
    print('Loading node data')    
    with arcpy.da.SearchCursor(pathnodes,fields_node) as cursorno:
        for row in cursorno:
             FID_n.append(row[2])
             all_FID_n.append(row[2])
             row_nodes.append(row) #to generate nodecon
             names_n.append(str_robust(row[1]))
             all_names_n.append(str_robust(row[1]))
             nodecoor.append(row[0].firstPoint)
             n_diameters.append(row[3])
             n_inlev.append(row[4])
             n_ground.append(row[5])
             typeNo.append(row[6])
             
    print('node-data loaded')
    
    # find outlets:
    [outlets.append(names_n[i]) for i in range(len(typeNo)) if typeNo[i]==3]
    
    #### CATCHMENT DATA ####
    print('Loading catchment data')
    with arcpy.da.SearchCursor(pathcatch,fields_catchcon) as cursorca:
        for row in cursorca:
            FID_i.append(row[0])
            catch_muid.append(row[1])
            node_inlet.append(str(row[2]))
            all_node_inlet.append(str(row[2]))
    

    with arcpy.da.SearchCursor(pathcatchconlink,fields_catchconlink) as cursorclink:
        for row in cursorclink:
            FID_i_link.append(row[0])
            catch_line.append(row[2])
        
    for i in range(len(node_inlet)): 
        newconnodes.append(node_inlet[i])
        oldconnodes.append(node_inlet[i])
     
    
    [t_adj.append([]) for i in range(len(catch_muid))]      # time adjustment for each catchment
    [pipe_adj.append([]) for i in range(len(catch_muid))]   # list of pipes that is removed on flowpath to catchment
    [c_emptylist.append([]) for i in range(len(catch_muid))] 
    
    
    
    #### NETWORK DATA ####
    # load network connections from text-file to lists:
    print('Loading network data')

    with open(pathnetconn, 'rb') as f:
        nodecon = pickle.load(f)
    
    for i in nodecon:
        linkage.append([int(i[0][0]),int(i[1][0]),int(i[2][0])])
        # if TrimMerge and not GenerateNetworkConnectivity:
            # linkage.append([int(i[0]),int(i[1]),int(i[2])])    #[pipe ID, startnode, endnode] 
        # else:
            # linkage.append([int(i[0][0]),int(i[1][0]),int(i[2][0])])    #[pipe ID, startnode, endnode] 
    
    node_s=[None]*(len(FID_p)) # start node of pipe
    node_e=[None]*(len(FID_p)) # end node of pipe
    all_node_s=[None]*(len(FID_p)) # start node of pipe
    all_node_e=[None]*(len(FID_p)) # end node of pipe
    
    for row in linkage:
        if str(row[0])in names:
            node_s[names.index(str(row[0]))]=str_robust(row[1])
            all_node_s[names.index(str(row[0]))]=str_robust(row[1])
            node_e[names.index(str(row[0]))]=str_robust(row[2])
            all_node_e[names.index(str(row[0]))]=str_robust(row[2])
    print('link-data loaded')
    

    #dictionaries of pipes connected each node with their start / end points
    for i in range(len(names)):
        name=names[i]
        start=node_s[i]
        end=node_e[i]        
        if not start in s_node_pipes:
            s_node_pipes[start]=[name]
            s_node_pipes_full[start]=[name]
        else:
            s_node_pipes[start].append(name)
            s_node_pipes_full[start].append(name)
            
        if not end in e_node_pipes:
            e_node_pipes[end]=[name]
            e_node_pipes_full[end]=[name]
        else:
            e_node_pipes[end].append(name)
            e_node_pipes_full[end].append(name)
    
    # sort outlets from diameter
    outdiameters=list()
    #make a list with end pipe diameters
    for i in outlets:
        try:
            outdiameters.append(diameters[names.index(s_node_pipes[i][0])])
        
        except:
            outdiameters.append(diameters[names.index(e_node_pipes[i][0])])
    # sort for largest diameter
    outlets=[x for y, x in sorted(zip(outdiameters, outlets),reverse=True)]      
    
    
    print('Load of data completed')
    
    return(geom, rows, FID_p, names, diameters, heights, widths, length, length_c, slope, manning, level_lo, level_up, crs, pipecoor_s, pipecoor_e, p_emtylist, p_emtylist2, c_emptylist, pipe_adj, all_names, all_diameters, all_length, all_slope, 
    new_uplevel, new_lolevel, new_polylines, new_dia, new_height, new_width, new_length, del_rows, 
    FID_n, all_FID_n, row_nodes, names_n, all_names_n, nodecoor, n_diameters, n_inlev, n_ground, outlets,
    FID_i, catch_muid, node_inlet, all_node_inlet, t_adj, newconnodes, oldconnodes, FID_i_link, catch_line, node_s, node_e, all_node_s, all_node_e, s_node_pipes, e_node_pipes, s_node_pipes_full, e_node_pipes_full,
    emptylist_1)
    
    